SYSTEM_ROOT=$$SYSROOT
AS=x86_64-unknown-lockdown-as
CC=x86_64-unknown-lockdown-gcc
CXX=x86_64-unknown-lockdown-g++
LD=x86_64-unknown-lockdown-gcc

CPPFLAGS=-fno-stack-protector -fno-stack-check -Wall -Wextra -fpic \
	   -I$(SYSTEM_ROOT)/System/Headers -g -fPIC

CFLAGS=$(CPPFLAGS) -std=gnu11 

MODULES=worstc lockdown linit sh
all: lockdown worstc wcrt linit sh

$(foreach m,$(MODULES),$(eval include $(m)/recipes.mk))
