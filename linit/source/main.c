#include <lockdown.h>
#include <power/ioctl.h>
#include <power/input.h>
#include <power/error.h>

#include <power/signal.h>

#include "terminal.h"

#define SSFN_IMPLEMENTATION
#include "ssfn.h"

int main()
{
    int input = OpenFile("\\??:/input", 0);
    struct terminal* t = utCreate();
    utWrite(t, "> ");

    int status = 0;
    struct input_event ev;
    while ((status = ReadFile(input, &ev, sizeof(ev))) > 0 
         || status == -ERROR_INTERRUPTED) {
        if (status == -ERROR_INTERRUPTED) {
            continue;
        }

        utHandleInputEvent(t, &ev);
    }
}
