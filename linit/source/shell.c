#include "shell.h"
#include "lockdown/file.h"
#include "terminal.h"

#include <power/types.h>
#include <lockdown/processthreads.h>

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

static void shReader(struct shell* sh)
{   
    int rd = 0;
    char* buffer = calloc(512, sizeof(char));
    while ((rd = ReadFile(sh->sh_output, buffer, 512)) > 0) {
        utWritel(sh->sh_term, buffer, rd);
    }

    free(buffer);
}

int shSpawnShell(struct shell* sh)
{
    sh->sh_input = CreatePipe(0);

    /* A packet-based communication just to transmit a single
       byte at once is so ill-fitting... But I haven't implemented
       sockets yet. */
    sh->sh_output = CreatePipe(0);

    struct _create_process_ex cpex;
    cpex.CommandLineArgs = (uint64_t)NULL;

    cpex.StderrHandle = sh->sh_output;
    cpex.StdoutHandle = sh->sh_output;
    cpex.StdinHandle = sh->sh_input;

    sh->sh_process = CreateProcess2("A:/System/sh.elf", &cpex);
    if (sh->sh_process < 0) {
        utWrite(sh->sh_term, "FAILED TO SPAWN SHELL");
        return sh->sh_process;
    }

    sh->sh_rdthr = CreateThread(shReader, sh, 0);
    if (sh->sh_rdthr < 0) {
        return sh->sh_rdthr;
    }

    return 0;
}
