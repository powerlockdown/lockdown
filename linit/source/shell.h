/* shell.h
   Purpose: Interaction with shell. */
#pragma once

#include <power/types.h>

struct terminal;

struct shell {
    __handle sh_output; /*< stderr, stdin */
    __handle sh_input; /*< stdin */
    __handle sh_process;

    __handle sh_rdthr;

    struct terminal* sh_term;
};

int shSpawnShell(struct shell*);
