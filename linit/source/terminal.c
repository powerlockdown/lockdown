#include "terminal.h"
#include "lockdown.h"
#include "lockdown/file.h"
#include "power/input.h"
#include "shell.h"
#include "translat_keys.h"
#include <stdint.h>

#include "power/ioctl.h"
#include "power/video/framebuffer.h"
#include "ssfn.h"
#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/mman.h>

#undef KEY_NO_POLLUTE
#include <power/input_code.h>

static void* loadFont()
{
  FILE* f = fopen("A:/System/Resources/Fonts/cleagleiii.ssfn2", "r");
  fseek(f, 0, SEEK_END);
  size_t fileSize = ftell(f);
  fseek(f, 0, SEEK_SET);
    
  void* buffer = malloc(fileSize);
  fread(buffer, fileSize, sizeof(char), f);
  fclose(f);

  return buffer;
}

struct terminal* utCreate()
{
  struct video_framebuffer vfb;
  struct terminal* tr = calloc(1, sizeof(struct terminal));
  tr->Font = loadFont();

  IoControl(0, IOCTL_ETERM_STOP_PRINTING, NULL, 0);

  int fb = open("\\??:/fb1", O_RDONLY);
  IoControl(fb, FB_GET_TOPOLOGY, &vfb, sizeof(vfb));
  vfb.Address = (uintptr_t)mmap(NULL, 0, PROT_WRITE, 0, fb, 0);

  tr->RenderBuffer.bg = 0xFFAAAAAA;
  tr->RenderBuffer.fg = 0xFFFFFFFF;
  tr->RenderBuffer.ptr = (void*)vfb.Address;
  tr->RenderBuffer.h = vfb.Height;

  tr->RenderBuffer.w = vfb.Width;
  tr->RenderBuffer.p = vfb.Pitch;
  tr->RenderBuffer.x = 5;
  tr->RenderBuffer.y = 20;

  tr->Context = calloc(1, sizeof(ssfn_t));
  ssfn_load(tr->Context, tr->Font);
  ssfn_select(tr->Context, SSFN_FAMILY_ANY, NULL, SSFN_STYLE_REGULAR, 16);

  int ignored;
  ssfn_bbox(tr->Context, "w", &tr->BackspaceSize, &ignored, &ignored, &ignored);

  tr->Shell = calloc(1, sizeof(struct shell));
  tr->Shell->sh_term = tr;
  shSpawnShell(tr->Shell);

  return tr;
}

void utWrite(struct terminal* t, const char* message)
{
  char msg[2] = { 0 };
  while (*message) {
    msg[0] = *message;
    ssfn_render(t->Context, &t->RenderBuffer, msg);
    message++;
  }
}

void utWritel(struct terminal* t, const char* msg, size_t length)
{
  char data[2] = {0, 0};
  while (length) {
    data[0] = *msg;
    ssfn_render(t->Context, &t->RenderBuffer, data);
    
    msg++;
    length--;
  }
}

void utHandleInputEvent(struct terminal* t, struct input_event* ev)
{
  if (!trlUpdateKeyboardStatus(t, ev))
    return;
  if (ev->Type != EV_INPUT_KEYPRESS)
    return;

  char cc;
  if ((cc = trlGetChar(t, ev->Code))) {
    char bf[2] = { cc, '\0' };

    WriteFile(t->Shell->sh_input, bf, 1);
    utWrite(t, bf);
  }

  if (ev->Code == KEY_BACKSPACE) {
    if (t->RenderBuffer.x > 5) {
      t->RenderBuffer.x -= t->BackspaceSize;
    }
  }
}
