#pragma once

#include "power/input.h"
#include <stddef.h>

#include "ssfn.h"

struct terminal {
    ssfn_buf_t RenderBuffer;
    ssfn_t* Context;

    void* Font;
    char* Buffer;
    int BufferIndex;
    int BackspaceSize;

#define TERMINAL_KB_SHIFT_BIT (1 << 0)
#define TERMINAL_KB_ALT_BIT   (1 << 1)
#define TERMINAL_KB_CAPS_LOCK_BIT (1 << 2)
    int KeyboardFlags;
  
    struct shell* Shell;
};

struct terminal* utCreate();
void utWrite(struct terminal* t, const char* message);
void utHandleInputEvent(struct terminal* t, struct input_event* ev);
void utWritel(struct terminal* t, const char* msg, size_t length);
