#include "translat_keys.h"
#include "power/input.h"
#include "terminal.h"
#include <ctype.h>
#include <power/input_code.h>

static char kbSet[255] = {
    [KEY_A] = 'a',
    [KEY_B] = 'b',
    [KEY_C] = 'c',
    [KEY_D] = 'd',
    [KEY_E] = 'e',
    [KEY_F] = 'f',
    [KEY_G] = 'g',
    [KEY_H] = 'h',
    [KEY_I] = 'i',
    [KEY_J] = 'j',
    [KEY_K] = 'k',
    [KEY_L] = 'l',
    [KEY_M] = 'm',
    [KEY_N] = 'n',
    [KEY_O] = 'o',
    [KEY_P] = 'p',
    [KEY_Q] = 'q',
    [KEY_R] = 'r',
    [KEY_S] = 's',
    [KEY_T] = 't',
    [KEY_U] = 'u',
    [KEY_V] = 'v',
    [KEY_W] = 'w',
    [KEY_X] = 'x',
    [KEY_Y] = 'y',
    [KEY_Z] = 'z',
    [KEY_SPACE]= ' ',
    [KEY_RETURN] = '\n',
    [KEY_RETURN_NUMP] = '\n',

    [KEY_1] = '1',
    [KEY_2] = '2',
    [KEY_3] = '3',
    [KEY_4] = '4',
    [KEY_5] = '5',
    [KEY_6] = '6',
    [KEY_7] = '7',
    [KEY_8] = '8',
    [KEY_9] = '9',
    [KEY_0] = '0',

    [KEY_1_NUMP] = '1',
    [KEY_2_NUMP] = '2',
    [KEY_3_NUMP] = '3',
    [KEY_4_NUMP] = '4',
    [KEY_5_NUMP] = '5',
    [KEY_6_NUMP] = '6',
    [KEY_7_NUMP] = '7',
    [KEY_8_NUMP] = '8',
    [KEY_9_NUMP] = '9',
    [KEY_0_NUMP] = '0',

    [KEY_HYPHEN] = '-',
    [KEY_EQUAL] = '=',
    [KEY_SEMICOLON] = ';',
    [KEY_LEFT_BR] = '[',
    [KEY_RIGHT_BR] = ']',
    [KEY_SLASH] = '\\',

    [KEY_ASTERISK_NUMP] = '*',
    
    [KEY_DOT] = '.',
    [KEY_DOT_NUMP] = '.',

    [KEY_FSLASH] = '/',
    [KEY_FSLASH_NUMP] = '/',

    [KEY_PLUS] = '+',
    [KEY_PLUS_NUMP] = '+',
};

/* kbSet when the shift key is on hold. */
static char alternKbSet[255] = {
  [KEY_BACKTICK] = '~',
  [KEY_1] = '!',
  [KEY_2] = '@',
  [KEY_3] = '#',
  [KEY_4] = '$',
  [KEY_5] = '%',
  [KEY_6] = '^',
  [KEY_7] = '&',
  [KEY_8] = '*',
  [KEY_9] = '(',
  [KEY_0] = ')',
  
  [KEY_HYPHEN] = '_',
  [KEY_EQUAL] = '=',
  [KEY_LEFT_BR] = '{',
  [KEY_RIGHT_BR] = '}',
  [KEY_SLASH] = '|',
  [KEY_SEMICOLON] = ':',
  [KEY_APOSTOPHR] = '"',
  [KEY_COMMA] = '<',
  [KEY_DOT] = '>',
  [KEY_FSLASH] = '?',
};

/* Get the character representation of
   keycode *key*, based on the terminal
   modifier keys state. */
int trlGetChar(struct terminal* term, int key)
{
  char item;
  char* decay = kbSet;
  if (term->KeyboardFlags & TERMINAL_KB_SHIFT_BIT) {
    decay = alternKbSet;
  }

  item = decay[key];
  if (!item && decay != kbSet) {
    item = toupper(kbSet[key]);
  }

  if (term->KeyboardFlags & TERMINAL_KB_CAPS_LOCK_BIT) {
    item = toupper(item);
  }
    
  return item;
}

/* Changes the kbd flags based on the supplied input event.
   Returns 0 if the state was modified. non-zero otherwise */
int trlUpdateKeyboardStatus(struct terminal* term,
                            const struct input_event* event)
{
  int bit = 0;
  switch(event->Code) {
  case KEY_LSHIFT:
  case KEY_RSHIFT:
    bit |= TERMINAL_KB_SHIFT_BIT;
    break;
  case KEY_LALT:
  case KEY_RALT:
    bit |= TERMINAL_KB_ALT_BIT;
    break;
  case KEY_CAPSLOCK:
    bit |= TERMINAL_KB_CAPS_LOCK_BIT; 
    break;
  }

  if (!bit)
    return 1; /* handle it for us, please. */
  
  if (event->Type == EV_INPUT_KEYPRESS) {
    term->KeyboardFlags |= bit;
  } else if (event->Type == EV_INPUT_KEYRELEASE) {
    term->KeyboardFlags &= ~bit;
  }

  return 0; /* Do not handle. */
}
