/* translat_keys.h
   Purpose: Convert KEY_* macros into something
            acceptable by the shell.  */
#pragma once

#include <power/input_code.h>
#include "terminal.h"

int trlGetChar(struct terminal*, int key);

int trlUpdateKeyboardStatus(struct terminal* term,
                            const struct input_event* event);
