/* file.c
   Purpose: Wrapper for system calls in File API */
#include <lockdown.h>
#include <power/system.h>
#include <stddef.h>
#include <stdint.h>

#include <power/filestats.h>
#include "lockdown.h"

int OpenFile(const char* name, int flags)
{
    return SystemCall(SYS_OPENFILE, (long)name, flags, 0, 0);
}

int WriteFile(int fh, const void* buffer, size_t size)
{
    return SystemCall(SYS_WRITEFILE, fh, (long)buffer, size, 0);
}

void CloseFile(int handle)
{
    SystemCall(SYS_CLOSEFILE, (long)handle, 0, 0, 0);
}

int ReadFile(int fh, void* buffer, size_t size)
{
    return SystemCall(SYS_READFILE, fh, (uint64_t)buffer, size, 0);
}

unsigned long SetFilePointer(int fh, int rel, size_t offset)
{
    return SystemCall(SYS_SETFILEPOINTER, fh, rel, offset, 0);
}

unsigned long IoControl(int device, int command,
                        void* buffer, unsigned long bufferSize)
{
    return SystemCall(SYS_DEVICEIOCTL, device, command, (uint64_t)buffer, bufferSize);
}

int MountVolume(int pt, const char* fsType, void* reserved)
{
    return SystemCall(SYS_MOUNTVOLUME, pt, (uint64_t)fsType, (uint64_t)reserved, 0);
}

int GetFileStatistics(int file, struct __filestats *fs)
{
  return SystemCall(SYS_GETFILESTATS, file, (long)fs, 0, 0);
}
