/* handle.c
   Purpose: for handle-generic operations */
#include "lockdown.h"
#include "power/system.h"
#include "lockdown/handle.h"

int GetHandleType(int fd)
{
  return SystemCall(SYS_GETHANDELTYPE, fd, 0, 0, 0);
}

int DuplicateHandle(int fd)
{
  return SystemCall(SYS_DUPHANDLE, fd, 0, 0, 0);
}
