/* lockdown.h
   Purpose: Syscall wrappers. Enjoy syscalling! */
#pragma once

#include <power/system.h>
#include <power/ioctl.h>

#ifndef __LOCKDOWN_H
#define __LOCKDOWN_H

#include "lockdown/file.h"
#include "lockdown/memory.h"

#ifndef LOCKDOWN_TRIMMED
#include "lockdown/processthreads.h"
#include "lockdown/signal.h"
#endif

extern long SystemCall(long arg1, long arg2, 
                       long arg3, long arg4, 
                       long arg5);


#endif /* defined(__LOCKDOWN_H) */
