/* file.h
   Purpose: Perform operations on files/pseudofiles. */
#pragma once

#ifndef __LOCKDOWN_FILE_H
#define __LOCKDOWN_FILE_H 1

#include "lockdef.h"
#include <power/filestats.h>

__LOCKDOWN_EXTERN_C_BEGIN

extern int OpenFile(const char* name, 
                      int flags);

extern int WriteFile(int fh, 
                     const void* buffer, 
                     unsigned long size);

extern int ReadFile(int fh,
                    void* buffer,
                    unsigned long size);

extern unsigned long SetFilePointer(int fh,
                                    int rel,
                                    unsigned long offset);

extern void CloseFile(int handle);

extern int MountVolume(int partition, 
                       const char* fsType, 
                       void* reserved);

extern unsigned long IoControl(int device,
                               int command,
                               void* buffer,
                               unsigned long bufferSize);
extern int GetFileStatistics(int file, struct __filestats *fs);

__LOCKDOWN_EXTERN_C_END

#endif
