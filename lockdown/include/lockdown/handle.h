/* handle.h */
#pragma once

#ifndef __LOCKDOWN_HANDLE_H
#define __LOCKDOWN_HANDLE_H 1

#include "lockdef.h"

__LOCKDOWN_EXTERN_C_BEGIN

extern int GetHandleType(int fd);
extern int DuplicateHandle(int fd);

__LOCKDOWN_EXTERN_C_END

#endif
