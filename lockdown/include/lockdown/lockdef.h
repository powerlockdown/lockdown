/* lockdef.h
   Purpose: macros */
#pragma once

#ifndef __LOCKDOWN_LOCKDEF_H
#define __LOCKDOWN_LOCKDEF_H 1

#ifdef __cplusplus
#define __LOCKDOWN_EXTERN_C_BEGIN extern "C" {
#define __LOCKDOWN_EXTERN_C_END   }
#else
#define __LOCKDOWN_EXTERN_C_BEGIN
#define __LOCKDOWN_EXTERN_C_END
#endif

typedef unsigned long __lkd_PTR;

typedef int __lkd_HANDLE;

#endif
