/* memory.h
   Purpose: map and unmap memory */
#pragma once

#ifndef __LOCKDOWN_MEMORY_H
#define __LOCKDOWN_MEMORY_H 1

#include "lockdef.h"

extern void* VirtualMap(__lkd_PTR address, 
                        unsigned long size, 
                        int flags);

extern void VirtualUnmap(__lkd_PTR address, 
                         unsigned long size);
extern void* VirtualFileMap(__lkd_HANDLE handle, 
                            __lkd_PTR hint, 
                            unsigned long length, 
                            int flags);

#endif
