/* processthreads.h 
   Purpose: Create processes or threads */
#pragma once

#ifndef __LOCKDOWN_PROCESS_THREADS_H
#define __LOCKDOWN_PROCESS_THREADS_H 1

#include "lockdef.h"

#include <power/system.h> /* _create_process_ex */

__LOCKDOWN_EXTERN_C_BEGIN

extern int CreateProcess(const char* program, 
                         int reserved, 
                         const char* commandLine,
                         int stdoutHandle);

extern int CreateProcess2(const char* program, 
                          struct _create_process_ex* cpex);

extern int CreatePipe(int reserved);

extern int SetExitCode(int exitcode);
extern int GetExitCode(int process);

extern int GetCurrentWorkingDir(char* buffer, 
                                unsigned long length);
extern int SetWorkingDirectory(const char* path);
extern void ExitProcess();

extern void WaitForObject(int handle);

extern int CreateThread(void* proc, void* param, 
                        unsigned long reserved);
extern int GetCurrentThreadId();
extern int KillThread(int id);

extern int SuspendThread(int thread);
extern int UnsuspendThread(int thread);

__LOCKDOWN_EXTERN_C_END

#endif
