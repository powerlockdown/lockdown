/* signal.h
   Purpose: signaling API */
#pragma once

#ifndef __LOCKDOWN_SIGNAL_H
#define __LOCKDOWN_SIGNAL_H 1

#include "lockdef.h"
#include <power/signal.h>

__LOCKDOWN_EXTERN_C_BEGIN

extern void
SigHandler(int signal, __sig_handler_pfn_t);

extern int
RaiseSignal(int signal);

__LOCKDOWN_EXTERN_C_END

#endif
