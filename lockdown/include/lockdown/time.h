/* time.h
   Purpose: time/calendar. */
#pragma once

#ifndef __LOCKDOWN_TIME_H
#define __LOCKDOWN_TIME_H 1

#include <power/time.h>
#include "lockdef.h"

__LOCKDOWN_EXTERN_C_BEGIN

typedef struct __wallclock WALLCLOCK;

extern int
GetTimeOfDay(WALLCLOCK* walc);

__LOCKDOWN_EXTERN_C_END

#endif
