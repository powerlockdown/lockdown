/* mem.c 
   Purpose: wrapper for memory syscalls */
#include "power/system.h"

#include <lockdown.h>
#include <stdint.h>
#include <stddef.h>

void* VirtualMap(uint64_t address, size_t size, int flags)
{
    return (void*)SystemCall(SYS_MAPPAGES, address, size, flags, 0);
}

void VirtualUnmap(uint64_t address, size_t size)
{
    SystemCall(SYS_UNMAPPAGES, address, size, 0, 0);
}

void* VirtualFileMap(int handle, uintptr_t hint, size_t length, int flags)
{
    return (void*)SystemCall(SYS_VIRTUALFILEMAP, handle, hint, length, flags);
}
