#include "lockdown.h"
#include "lockdown/processthreads.h"

#include <power/ioctl.h>
#include <power/system.h>
#include <power/types.h>
#include <power/limits.h>
#include <stddef.h>

int CreateProcess(const char* program, int reserved, 
                  const char* commandLine, int stdoutHandle)
{
    struct _create_process_ex ex = { (__uint64)commandLine, 
                                stdoutHandle, INVALID_HANDLE, INVALID_HANDLE,
                                0 };
    return SystemCall(SYS_CREATEPROCESS, (long)program, reserved, (long)&ex, 0);
}

int SetExitCode(int exitcode)
{
    return IoControl(0, IOCTL_SET_EXIT_CODE, (void*)(__uint64)exitcode, sizeof(int));
}

int GetExitCode(int process)
{
    return IoControl(process, IOCTL_GET_EXIT_CODE, NULL, 0);
}

int GetCurrentWorkingDir(char* buffer, unsigned long length)
{
    return SystemCall(SYS_GETCURRENTWD, (unsigned long)buffer, length, 0, 0);
}

void ExitProcess()
{
    SystemCall(SYS_EXITPROCESS, 0, 0, 0, 0);
}

void WaitForObject(int handle)
{
    IoControl(handle, IOCTL_WAIT_FOR_OBJECT, NULL, 0);
}

int CreatePipe(int reserved)
{
    return SystemCall(SYS_CREATEPIPE, reserved, 0, 0, 0);
}

static void __thread_tramp()
{
  KillThread(GetCurrentThreadId());
}

int CreateThread(void* proc, void* param, unsigned long reserved)
{
    (void)reserved;
    return SystemCall(SYS_CREATETHREAD, (long)proc, (long)param, 0, (long)__thread_tramp);
}

int GetCurrentThreadId()
{
    return SystemCall(SYS_GETCRTHREADID, 0, 0, 0, 0);
}

int KillThread(int handle)
{
    return SystemCall(SYS_KILLTHREAD, handle, 0, 0, 0);
}

int CreateProcess2(const char* program, 
                   struct _create_process_ex* cpex)
{
    return SystemCall(SYS_CREATEPROCESS, (long)program, 0, (long)cpex, 0);
}

int SetWorkingDirectory(const char* path)
{
    return SystemCall(SYS_SETWORKINGDIR, (long)path, 0, 0, 0);
}

int SuspendThread(int thread)
{
  return SystemCall(SYS_SETTHRSUSPND, thread, 1, 0, 0);
}

int UnsuspendThread(int thread)
{
  return SystemCall(SYS_SETTHRSUSPND, thread, 0, 0, 0);
}
