#include "lockdown.h"
#include "power/system.h"
#include <power/signal.h>

void SigHandler(int signal, __sig_handler_pfn_t h)
{
    SystemCall(SYS_SIGHANDLER, signal, (long)h, 0, 0);
}

int RaiseSignal(int signal)
{
    return SystemCall(SYS_RAISESIGNAL, signal, 0, 0, 0);
}
