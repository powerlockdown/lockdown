#include <lockdown/time.h>
#include <power/system.h>

#include "lockdown.h"
#include "syscall.h"

int GetTimeOfDay(WALLCLOCK* walc)
{
    return SystemCall(SYS_GETTIMEOFDAY, (long)walc, 0, 0, 0);
}
