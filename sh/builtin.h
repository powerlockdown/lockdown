/* builtin.h
   Purpose: built-in definition. */
#pragma once

#include <string>
#include <string_view>
#include <map>

class shell;

using builtin_handler = int (*)(shell*, std::string_view args);

class builtin_registry {
public:
    static 
    void register_builtin(std::string_view name, builtin_handler handler)
    {
        auto& br = get_instance();
        auto nmstr = std::string(name);
        
        br.m_handlers.insert_or_assign(nmstr, handler);
    }

    static
    builtin_handler get_builtin(const std::string& name)
    {
        auto& br = get_instance();
        auto find = br.m_handlers.find(name);
        if (find == br.m_handlers.end())
            return nullptr;

        return find->second;
    }

    static
    builtin_handler get_builtin(std::string_view name)
    {
        std::string str = std::string(name);
        return get_builtin(str);
    }

    static builtin_registry& get_instance()
    {
        static builtin_registry br;
        return br;
    }

private:
    builtin_registry() = default;

    std::map<std::string, builtin_handler> m_handlers;
};

/* N - name, f - builtin routine */
#define REGISTER_BUILTIN(N, b) __attribute__((constructor)) \
static void __static_register_ ## N () \
{ builtin_registry::register_builtin(#N , b); }
