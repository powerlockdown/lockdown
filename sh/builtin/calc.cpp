/* calc.cpp
   Purpose: perform arithmetic calculation */
#include "builtin.h"
#include "shell.h"

#include <charconv>
#include <functional>
#include <stdint.h>
#include <list>
#include <stdio.h>

enum {
  TOKEN_NONE,

  TOKEN_INTEGER,
  
  TOKEN_PLUS,
  TOKEN_MINUS,
  TOKEN_ASTERISK,
  TOKEN_SLASH,
  TOKEN_PERCENT
};

struct expr_token {
  int ext_type;
  union {
    intmax_t ext_extraint;
    std::string_view ext_extrastr;
  };

  expr_token(int type)
    : ext_type(type)
  {}

  expr_token(int type, intmax_t number)
    : ext_type(type),
      ext_extraint(number)
  {}
};

struct expr_lexer {
  std::string_view exl_input;
  std::list<expr_token> exl_tokens;

  expr_lexer(std::string_view input);
};

expr_lexer::expr_lexer(std::string_view input)
  : exl_input(input)
{
  intmax_t number;
  const char* begin = nullptr;
  const char* end = nullptr;
  for (const char& c : exl_input) {
    if (std::isdigit(c)) {
      if (!begin) {
	begin = &c;
      }

      if (&c + 1 == exl_input.end()) {
	goto beg;
      }
      
      continue;
    } else {
beg:
      if (begin) {
	end = &c;
	if (end == begin)
	  end++;
	
	std::from_chars(begin, end, number, 10);
	exl_tokens.emplace_back(TOKEN_INTEGER, number);
	
	begin = nullptr;
	end = nullptr;
      }
    }
    
    switch (c) {
    case '+':
      exl_tokens.emplace_back(TOKEN_PLUS);
      continue;
    case '-':
      exl_tokens.emplace_back(TOKEN_MINUS);
      continue;
    case '*':
      exl_tokens.emplace_back(TOKEN_ASTERISK);
      continue;
    case '/':
      exl_tokens.emplace_back(TOKEN_SLASH);
      continue;
    case '%':
      exl_tokens.emplace_back(TOKEN_PERCENT);
      continue;
    }
  }
}

struct expr_parser_node {
  int epn_type;
  intmax_t epn_value;
  
  struct expr_parser_node* epn_left;
  struct expr_parser_node* epn_right;

  expr_parser_node(int type, intmax_t value,
		   struct expr_parser_node* left,
		   struct expr_parser_node* right)
    : epn_type(type),
      epn_value(value),
      epn_left(left),
      epn_right(right)
  {}
  
  intmax_t exec_op()
  {
    switch (epn_type) {
    case TOKEN_INTEGER:
      return epn_value;
    case TOKEN_ASTERISK: {
      return exec_inner(std::multiplies<intmax_t>());
    }
    case TOKEN_MINUS: {
      return exec_inner(std::minus<intmax_t>());
    }
    case TOKEN_PLUS: {
      return exec_inner(std::plus<intmax_t>());
    }
    case TOKEN_SLASH: {
      return exec_inner(std::divides<intmax_t>());
    }
    }

    return 0;
  }

private:

  template<class _Ty>
  inline intmax_t exec_inner(_Ty op)
  {
    return op(epn_left->exec_op(), epn_right->exec_op());
  }
};

struct expr_parser {
  using iterator = std::list<expr_token>::iterator;
  
  expr_parser(const std::list<expr_token>& tokens)
    : exp_tokens(tokens),
      exp_index(exp_tokens.begin())
  {}

  intmax_t parse()
  {
    auto root = parse_subtraction();
    auto div = root->exec_op();
    return div;
  }

  std::list<expr_parser_node> exp_nodes;
  std::list<expr_token> exp_tokens;

  iterator exp_index;
  
private:
#define IS_CORRECT_TYPE(I, v, t) ((I) == (v).end() || (I)->ext_type != (t))

  expr_parser_node* parse_subtraction()
  {
    auto left = parse_addition();
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_MINUS)) {
      return left;
    }

    exp_index++;
    auto& middle = exp_nodes.emplace_back(exp_index->ext_type, 0, left, nullptr);

    exp_index++;
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_INTEGER)) {
      /* Error. */
      return &middle;
    }

    auto right = parse_addition();
    middle.epn_right = right;
    exp_index++;

    return &middle;
  }
  
  expr_parser_node* parse_addition()
  {
    auto left = parse_mult();
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_PLUS)) {
      return left;
    }

    exp_index++;
    auto& middle = exp_nodes.emplace_back(exp_index->ext_type, 0, left, nullptr);

    exp_index++;
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_INTEGER)) {
      /* Error. */
      return &middle;
    }

    auto right = parse_mult();
    middle.epn_right = right;
    exp_index++;

    return &middle;
  }
    
  expr_parser_node* parse_mult()
  {
    auto left = parse_div();
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_ASTERISK)) {
      return left;
    }

    exp_index++;
    auto& middle = exp_nodes.emplace_back(exp_index->ext_type, 0, left, nullptr);

    exp_index++;
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_INTEGER)) {
      /* Error. */
      return &middle;
    }

    auto right = parse_div();
    middle.epn_right = right;
    exp_index++;

    return &middle;
  }
    
  expr_parser_node* parse_div()
  {
    auto& left = exp_nodes.emplace_back(exp_index->ext_type, exp_index->ext_extraint,
					nullptr, nullptr);
    exp_index++;
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_SLASH)) {
      return &left;
    }

    auto& middle = exp_nodes.emplace_back(exp_index->ext_type, 0, &left, nullptr);
    exp_index++;
    if (IS_CORRECT_TYPE(exp_index, exp_tokens, TOKEN_INTEGER)) {
      /* Error. */
      return &middle;
    }

    auto& right = exp_nodes.emplace_back(exp_index->ext_type, exp_index->ext_extraint, nullptr, nullptr);
    middle.epn_right = &right;
    exp_index++;
    
    return &middle;
  }
};

static int calc(shell *, std::string_view arg)
{
  expr_lexer lexer(arg);
  expr_parser par(lexer.exl_tokens);
  auto result = par.parse();
  fprintf(stdout, "%i\n", result);
  
  return 0;
}

REGISTER_BUILTIN(calc, calc);
