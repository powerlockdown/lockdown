/* echo.cpp
   Purpose: tell the shell to print 
            an arbitrary message. */
#include "builtin.h"
#include <stdio.h>

static int echo(shell*, std::string_view args)
{
    auto data = std::string(args);
    data.append("\n");

    fputs(data.c_str(), stdout);
    return 0;
}

REGISTER_BUILTIN(echo, echo);
