/* pwd_cd.cpp
   Purpose: print the current working directory (pwd) and
            change the current working directory */
#include "builtin.h"

#include "shell.h"
#include "lockdown/processthreads.h"
#include <power/limits.h>

static
int pwd(shell*, std::string_view)
{
    char data[MAX_PATH];
    GetCurrentWorkingDir(data, MAX_PATH);

    fputs(data, stdout);
    fflush(stdout);
    return 0;
}

static
int cd(shell* sh, std::string_view arg)
{
    auto str = std::string(arg);
    SetWorkingDirectory(str.c_str());

    str.clear();
    str.reserve(MAX_PATH);
    
    GetCurrentWorkingDir(str.data(), MAX_PATH);
    sh->register_variable("CD", str);
    return 0;
}

__attribute__((constructor))
static void __static_register_pwdcd()
{
    builtin_registry::register_builtin("pwd", pwd);
    builtin_registry::register_builtin("cd", cd);
}

