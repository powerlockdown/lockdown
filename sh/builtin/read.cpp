/* read.cpp
   Purpose: read a file */
#include "builtin.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

static size_t getfsize(FILE* file)
{
    off_t off = ftell(file);
    size_t fsize;
    fseek(file, 0, SEEK_END);

    fsize = ftell(file);
    fseek(file, off, SEEK_SET);
    return fsize;
}

static int readb(shell*, std::string_view arg)
{
    auto data = std::string(arg);
    int fd = open(data.c_str(), O_RDONLY);
    if (fd == -1) {
        fprintf(stdout, "Error during read: %s\n", strerror(errno));
        return -1;
    }

    char* buffer;
    size_t size;
    FILE* file;
    
    file = fdopen(fd, "r");
    size = getfsize(file);
    buffer = new char[size];
    
    fread(buffer, size, sizeof(char), file);
    fputs(buffer, stdout);
    fputc('\n', stdout);

    delete[] buffer;
    fclose(file);
    return 0;
}

REGISTER_BUILTIN(read, readb);
