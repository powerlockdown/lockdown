#include <cstdio>
#include "shell.h"

int main()
{
    int c;
    class shell s;
    std::string prompt;
    
    while (!s.should_close()) {
        prompt = s.get_prompt();
	std::fputs(prompt.c_str(), stdout);
	std::fflush(stdout);
	s.set_read_input_flag(true);
	
        while (s.should_read_input()) {
            c = std::fgetc(stdin);
            s.handle_input(c);
        }
    }
}
