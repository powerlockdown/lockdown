/* notin.h
   Purpose: support for non-interactive usages. */
#pragma once

#include <string_view>

int source_script(struct shell*, std::string_view path);
