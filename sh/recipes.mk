SH_SOURCE_DIR=sh/
SH_BUILD_DIR=build/sh
SH_OUTPUT=$(SH_BUILD_DIR)/sh.elf

SH_FILES=main.cpp shell.cpp builtin/echo.cpp builtin/pwd_cd.cpp builtin/read.cpp
SH_SOURCES=$(addprefix $(SH_SOURCE_DIR)/,$(SH_FILES))
SH_BUILD=$(addprefix $(SH_BUILD_DIR)/,$(addsuffix .o,$(SH_FILES)))

SH_LDFLAGS=$(LDFLAGS) -lworstc -llockdown -lgcc_s
SH_CXXFLAGS=$(CPPFLAGS) -std=c++17 -Ish

$(SH_OUTPUT): $(SH_BUILD)
	$(CXX) $(SH_LDFLAGS) $(SH_BUILD) -o $(SH_OUTPUT)

$(SH_BUILD_DIR)/%.cpp.o: $(SH_SOURCE_DIR)/%.cpp
	mkdir -p $(dir $(subst $(SH_SOURCE_DIR)/,$(SH_BUILD_DIR)/,$^))
	$(CXX) $(SH_CXXFLAGS) $^ -c -o $@

sh: $(SH_OUTPUT)

sh_install: sh
	install -C -D -t $(SYSTEM_ROOT)/System/ $(SH_OUTPUT)
