#include "shell.h"
#include "builtin.h"
#include "lockdown/processthreads.h"

#include <array>
#include <string_view>
#include <algorithm>

#include <power/limits.h>
#include <lockdown/processthreads.h>

shell::shell()
    : m_shouldread(true),
      m_shouldclose(false)
{
    const char* ps1 = std::getenv("PS1");
    if (!ps1) {
        ps1 = "$CD $";
    }

    char path[MAX_PATH];
    GetCurrentWorkingDir(path, MAX_PATH);

    m_variables.insert_or_assign("CD", path);
    m_variables.insert_or_assign("ily", "ILOVEYOU");
    m_prompt = ps1;
}

int shell::execute_command(std::string_view command)
{
    int var;
    unsigned long id;
    builtin_handler bh;
    std::array<std::string_view, 2> args;
    std::string realstr;

    realstr = std::string(command);
    expand_dollars(realstr);

    id = realstr.find_first_of(' ');
    
    args[0] = realstr.substr(0, id);
    if (id != std::string_view::npos) {
        args[1] = realstr.substr(id + 1);
    }

    if (!(bh = builtin_registry::get_builtin(args[0]))) {
        /* Must point to a file. */
        goto file;
    }

    var = bh(this, args[1]);
    return var;
file:
    return 1;
}

void shell::handle_input(int ch)
{
    if (ch == '\n') {
        this->execute_command(m_buffer);
	m_buffer.shrink_to_fit();

	/* Redraw the prompt. */
	m_shouldread = false;
        return m_buffer.clear();
    }

    m_buffer += ch;
}

/* Register a variable from assignment syntax. */
void shell::register_input_variable(std::string_view varsyntx)
{
    /* Variable assignment syntax:
        <var> = <value>.
        e.g.: x=2  */

    std::string_view::size_type equ;
    if ((equ = varsyntx.find_first_of('=')) == std::string_view::npos) {
        return;
    }

    auto key = varsyntx.substr(0, equ);
    auto value = varsyntx.substr(0, equ + 1);

    m_variables.insert_or_assign(std::string(key), std::string(value));
}

/* Handle all occurrences of dollars inside the phrase. */
void shell::expand_dollars(std::string& phrase)
{
    char next;

    std::string name;
    std::string::size_type spc;
    std::string::size_type last = 0;
    std::string::size_type dlidx;

    while ((dlidx = phrase.find_first_of('$', last)) != std::string::npos) {
        next = phrase[dlidx + 1];
        if (!std::isalpha(next) && next != '$') {
            last = dlidx + 1;
            continue;
        }

        if (std::isalpha(next)) {
            spc = phrase.find(' ', dlidx + 1);
            name = phrase.substr(dlidx + 1, spc);
	    spc = std::clamp(spc, spc, std::string::npos - 1);
	    
            phrase.erase(dlidx, spc + 1);
            phrase.insert(dlidx, get_variable(name));
        }
    }
}

/* Expand and return the prompt. */
std::string shell::get_prompt()
{
  std::string str = m_prompt;
  expand_dollars(str);
  
  return str;
}
