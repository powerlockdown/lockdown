/* shell.h
   Purpose: the main */
#pragma once

#include <map>
#include <string>

class shell {
public:
    shell();

    bool should_close() const
    {
        return m_shouldclose;
    }

    bool should_read_input() const 
    {
        return m_shouldread;
    }

    void register_variable(std::string_view key, std::string_view value)
    {
        m_variables.insert_or_assign(std::string(key), std::string(value));
    }

    void register_variable(std::string key, std::string value)
    {
        m_variables.insert_or_assign(key, value);
    }

    std::string_view get_variable(const std::string& key)
    {
        auto iter = m_variables.find(key);
        if (iter == m_variables.end()) {
            return "";
        }

        return iter->second;
    }

    std::string get_prompt();

    void set_read_input_flag(bool a)
    {
       m_shouldread = a;
    }
  
    void handle_input(int ch);
    int execute_command(std::string_view program);

private:
    void expand_dollars(std::string& data);
    void register_input_variable(std::string_view varsyntx);

    bool m_shouldread; /*< Should read input? */
    bool m_shouldclose;

    std::string m_buffer;
    std::string m_prompt;

    std::map<std::string, std::string> m_variables;
};
