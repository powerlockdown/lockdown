/* assert.c
   Purpose: the implementation of __assert_fail */
#include "assert.h"
#include "stdio.h"
#include "stdlib.h"

__noret
int __assert_fail(const char* expr, const char* file, int line)
{
    fprintf(stderr, "Expression '%s' happened to be false.", expr);
    fprintf(stderr, " Inside line %i of file %s\n", line, file);
    fflush(stderr);
    
    abort();
}

