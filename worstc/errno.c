/* errno.c
   Purpose: the definitition of internal 
            errno variable and _get_errno */
#include "errno.h"
#include "stdlib.h"
#include "bits/pthread.h"

#include <power/error.h>

int* _get_errno()
{
  return &__pthread_self()->pth_errno;
}

void __set_errno(int e)
{
    errno = e;
}

/* Convert a kernel error code onto a POSIX one. */
int __ecode_into_errno(int ecode)
{
    ecode = abs(ecode);
    switch (ecode) {
    case ERROR_OK:
        return 0;
    case ERROR_NOT_FOUND:
        return ENOENT;
    case ERROR_NO_SYSCALL:
        return ENOSYS;
    case ERROR_NO_DEVICE:
        return ENODEV;
    case ERROR_INVALID_RANGE:
        return ERANGE;
    case ERROR_FORBIDDEN:
        return EPERM;
    case ERROR_TIMED_OUT:
        return ETIMEDOUT;
    case ERROR_IO:
        return EIO;
    case ERROR_NOT_SUPPORTED:
        return EOPNOTSUPP;
    case ERROR_PIPE_BROKEN:
        return EPIPE;
    case ERROR_BAD_HANDLE:
        return EBADF;
    case ERROR_ALREADY_EXISTS:
        return EEXIST;
    case ERROR_NO_MEMORY:
        return ENOMEM;
    case ERROR_UNAVAILABLE:
        return EBUSY;
    case ERROR_WOULD_BLOCK:
        return EWOULDBLOCK;
    case ERROR_USER_FAULT:
        return EFAULT;
    case ERROR_TOO_MANY_HANDLES:
        return EMFILE;
    case ERROR_INVALID_EXECUTABLE:
        return ENOEXEC;
    case ERROR_INTERRUPTED:
        return EINTR;
    }

    return EINVAL;
}
