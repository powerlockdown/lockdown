/* fini.c
   Purpose: destructor calling */
#include "bits/fini.h"

/* TODO port this to use atexit instead. */

typedef void(*PFDV)(void);

static PFDV __finibegin;
static PFDV __finiend;

void __wc_set_fini_array(void* begin, void* end)
{
    __finibegin = begin;
    __finiend = end;
}

void __wc_call_dtors()
{
    PFDV dtor = __finibegin;
    while (dtor != __finiend) {
        dtor();
        dtor++;
    }
}
