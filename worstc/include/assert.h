/* assert.h
   Purpose: bail out if condition is false. */
#pragma once

#include "bits/config.h"

#ifndef __ASSERT_H 
#define __ASSERT_H 1

__WC_EXTERN_C_BEGIN

__noret
extern int __assert_fail(const char* expr, const char* file, int line);

__WC_EXTERN_C_END

#ifdef NDEBUG
#define assert(X)
#else
#define assert(X) ((X) ? 0 : __assert_fail( #X , __FILE__ , __LINE__ ))
#endif

#endif
