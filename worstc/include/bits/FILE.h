/* FILE.h
   Purpose: The definition of FILE (internally). */
#pragma once

#ifndef __BITS_FILE_H
#define __BITS_FILE_H 1

#define __need_size_t
#include <stddef.h>

#define __FILE_EOF        0x00000001
#define __FILE_ERROR      0x00000010
#define __FILE_WRITE_ONLY 0x00000100
#define __FILE_STATIC     0x00001000

#define __FBUFFERING_NONE 0 /*< No buffering */
#define __FBUFFERING_LINE 1 /*< Line buffering */
#define __FBUFFERING_BUFFER 2 /*< Full bufferring */
#define __FBUFFERING_OWNERSHIP (1 << 10) /*< We are responsible for freeing f_bfr_buffer. */

struct _FILE_struct {
    /* ungetc data. */
    char* f_buffer_stack;
    size_t f_bs_index, f_bs_pos;
    size_t f_position;

    /* Buffering metadata */
    size_t f_bfr_position;
    size_t f_bfr_length;
    char* f_bfr_buffer;
    int f_bfr_mode; /*< One of __FBUFFERING_*. */

    /* Data used by everyone. */
    int f_handle;
    int f_flags;
};

int __pop_buffer_char(struct _FILE_struct* stream);

#endif

