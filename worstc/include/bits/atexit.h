/* atexit.h
   Purpose: register callbacks to be executed
            during program termination */
#pragma once

#ifndef __BITS_ATEXIT_H
#define __BITS_ATEXIT_H 1

#include "bits/config.h"

__WC_EXTERN_C_BEGIN

typedef void (*__atexit)();
int atexit(__atexit _proc);

void __atex_dispatch_handlers() __WC_hidden;

__WC_EXTERN_C_END

#endif
