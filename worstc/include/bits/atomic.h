/* atomic.h
   Purpose: atomic wrappers */
#pragma once

#ifndef __BITS_ATOMIC_H
#define __BITS_ATOMIC_H 1

#define __wc_atomic_load_relaxed(P) __atomic_load_n(&(P), __ATOMIC_RELAXED)
#define __wc_atomic_store_relaxed(P, v)                                        \
  __atomic_store_n(&(P), v, __ATOMIC_RELAXED)

#endif
