/* div.h 
   Purpose: division */
#pragma once

#ifndef __BITS_DIV_H
#define __BITS_DIV_H 1

#include "bits/config.h"

__WC_EXTERN_C_BEGIN

typedef struct {
    int quot;
    int rem;
} div_t;

extern
div_t div(int _numer, int _denom);

typedef struct {
    long quot;
    long rem;
} ldiv_t;

extern
ldiv_t ldiv(long _numer, long _denom);

typedef struct {
    long long quot;
    long long rem;
} lldiv_t;

extern
lldiv_t lldiv(long long _numer, long long _denom);

__WC_EXTERN_C_END

#endif
