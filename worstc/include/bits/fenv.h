/* fenv.h
   Purpose: x86(x87) definitions of
            floating point environment */
#pragma once

#ifndef __BITS_FENV_H
#define __BITS_FENV_H 1

#include "stdint.h"

/* The macros defined here
   expand to their equivalents
   in MXCSR storage. */

#define FE_DIVBYZERO (1 << 2)
#define FE_OVERFLOW (1 << 3)
#define FE_UNDERFLOW (1 << 4)

#define FE_ALL_EXCEPT (FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW)

#define FE_TONEAREST 0
#define FE_DOWNWARD 1
#define FE_UPWARD 2
#define FE_TOWARDZERO 3

typedef uint32_t fenv_t;

#endif

