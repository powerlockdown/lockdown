/* it_pri.h
   Purpose: inttypes.h PRI macros */
#pragma once

#ifndef __BITS_IT_PRI_H
#define __BITS_IT_PRI_H 1

#include "stdint.h"

/* Pretend you are completely
   unaware of the compiler
   providing that header and
   be pessismistic. */

#define PRIi32 "i"
#define PRIu32 "u"
#define PRIo32 "o"
#define PRIx32 "x"
#define PRIX32 "X"

#define PRIi64 "li"
#define PRIu64 "lu"
#define PRIo64 "lo"
#define PRIx64 "lx"
#define PRIX64 "lX"

#endif
