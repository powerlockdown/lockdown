/* lklist.h
   Purpose: the implementation of a
            linked list */
#pragma once

#ifndef __BITS_LKLIST_H
#define __BITS_LKLIST_H 1

#include <stddef.h>
#include "config.h"

__WC_EXTERN_C_BEGIN

struct __lklist {
  struct __lklist* lkl_prev;
  struct __lklist* lkl_next;
};

static inline
void __klist_insert(struct __lklist* head,
                    struct __lklist* middle)
{
  struct __lklist* tmp = head;
  while(tmp->lkl_next)
    tmp = tmp->lkl_next;

  middle->lkl_prev = tmp;
  tmp->lkl_next = middle;
}

static inline
struct __lklist* __klist_remove(struct __lklist* item)
{
 struct __lklist* p = item->lkl_prev;
 struct __lklist* x = item->lkl_next;

 if (x)
   x->lkl_prev = p;
 if (p)
   p->lkl_next = x;

 return item->lkl_prev;
}

__WC_EXTERN_C_END

#endif
