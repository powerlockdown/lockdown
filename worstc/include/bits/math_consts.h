/* math_consts.h
   Purpose: Mathematical constants */
#pragma once

#ifndef __BITS_MATH_CONSTS_H
#define __BITS_MATH_CONSTS_H 1

/* These are not specified by the std,
   but glibc defines it and fdlibm wants them. */

/** Constant of Napier. The base of natural logs. */
#define M_E 2.718281828459045
#define M_El ((long double) 2.718281828459045)
#define M_Ef 2.718281828459045f

/** The logarithm to base 2 of M_E. */
#define M_LOG2E 1.4426950408889634
#define M_LOG2Ef 1.4426950408889634f
#define M_LOG2El ((long double)1.4426950408889634)

/** The logarithm to base 10 of M_E. */
#define M_LOG10E 0.4342944819032518
#define M_LOG10Ef 0.4342944819032518f 
#define M_LOG10El ((long double)0.4342944819032518)

/** The natural logarithm of 2.  */
#define M_LN2 0.6931471805599453
#define M_LN2f 0.6931471805599453f
#define M_LN2l ((long double)0.6931471805599453f)

/** The natural logarithm of 10. */
#define M_LN10 2.302585092994046
#define M_LN10f 2.302585092994046f
#define M_LN10l ((long double)2.302585092994046)

/** The ratio of a cicle's circumference to its diameter. */
#define M_PI 3.141592653589793
#define M_PIf 3.141592653589793f
#define M_PIl ((long double)3.141592653589793)

/** Pi, divided by two. */
#define M_PI_2 1.5707963267948966
#define M_PI_2f 1.5707963267948966f
#define M_PI_2l ((long double)1.5707963267948966)

/** Pi, divided by four. */
#define M_PI_4 0.7853981633974483
#define M_PI_4f 0.7853981633974483f
#define M_PI_4l ((long double)0.7853981633974483)

/** The reciprocal of pi. 1/pi */
#define M_1_PI 0.3183098861837907
#define M_1_PIf 0.3183098861837907f
#define M_1_PIl ((long double)0.3183098861837907)

/** Two times the reciprocal of pi. 2/pi */
#define M_2_PI 0.6366197723675814
#define M_2_PIf 0.6366197723675814f
#define M_2_PIl ((long double)0.6366197723675814)

/** Two times the reciprocal of the square root of pi. 2/sqrt(pi) */
#define M_2_SQRTPI 1.1283791670955126
#define M_2_SQRTPIf 1.1283791670955126f
#define M_2_SQRTPIl ((long double)1.1283791670955126)

/** The square root of two. */
#define M_SQRT2 1.4142135623730951
#define M_SQRT2f 1.4142135623730951f
#define M_SQRT2l ((long double)1.4142135623730951)

/** The reciprocal of the square root of two. 1/sqrt(2) (=> sqrt(1/2)) */
#define M_SQRT1_2 0.7071067811865476
#define M_SQRT1_2f 0.7071067811865476f
#define M_SQRT1_2l ((long double)0.7071067811865476)

#endif

