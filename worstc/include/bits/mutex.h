/* mutex.h
   Purpose: the definition of a pthread_mutex_t */
#pragma once

#ifndef __BITS_MUTEX_H
#define __BITS_MUTEX_H 1

#include "lklist.h"
#include "pthread_attr.h"

#define _PTHEV_OWNER_DIED_BIT (1 << 0)

struct __pthread_event {
  struct __lklist __ev_list;
  
  int __ev_waiter; /*< The thread which currently waits. */
  int __ev_flags;
};

#define _LIST_INITIALIZER {0, 0}
#define _PTHEV_INITIALIZER { _LIST_INITIALIZER, 0, 0 }

struct __pthread_thrqueue {
  int __tq_length;
  int __tq_capacity;

  int* __tq_buffer;
};

#define _PTQ_INITIALIZER { 0, 0, NULL }

typedef struct {
  struct __pthread_event __mtx_base;

  int __mtx_uses;
  int __mtx_owner;
  
  short int __mtx_mode;
  short int __mtx_robust;
  struct __pthread_thrqueue  __mtx_waiters;
} pthread_mutex_t;

#define _PMTX_INITIALIZER { _PTHEV_INITIALIZER, 0, 0, PTHREAD_MUTEX_NORMAL, PTHREAD_MUTEX_STALLED, _PTQ_INITIALIZER  };

typedef struct {
  int __rw_uses;
  int __rw_owner;
  
  int __rw_nreaders;
  int __rw_nwriters;

#define _RWLOCK_OWNER_WRITER (1 << 0) /*< Owner is a writer (1) or a reader (0) */
#define _RWLOCK_PREFER_WRITERS (1 << 0) /*< Prefer writers (set) or readers (unset) */
  int __rw_flags;
  
  struct __pthread_thrqueue __rw_readers;
  struct __pthread_thrqueue __rw_writers;
} pthread_rwlock_t;

#define _PRW_INITIALIZER { 0, 0, 0, 0, 0, _PTQ_INITIALIZER, _PTQ_INITIALIZER  }

typedef struct {
  int __on_status; /*< Atomic. */
  
} pthread_once_t;

int __tq_pop(struct __pthread_thrqueue*);
int __tq_push(struct __pthread_thrqueue*, int val);

void __pthread_event_odnotify(struct __pthread_event* __event);

#endif

