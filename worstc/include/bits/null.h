/* null.h
   Purpose: macros for syntactic sugar */
#pragma once

#ifndef __BITS_NULL_H
#define __BITS_NULL_H 1

#define __return_value(E, v) do { errno = (E); return v ; } while(0)
#define __return_enoc(E, v) __return_value(__ecode_into_errno(E), v)

#define __return_null(E) __return_value(E, NULL)
#define __return_void(E) __return_value(E, )

#endif
