/* pthread.h
   Purpose: definition of a thread.  */
#pragma once

#include "bits/lklist.h"
#include <stdint.h>

#define _PTHR_KEYS 512

struct __pthread {
  struct __pthread* pth_self; /*< Must be a pointer to this
			          structure.*/
  int pth_errno;
  int pth_id;
  
  void* pth_context;
  void* (*pth_entry)(void*);

  uintptr_t* pth_keyarray;
  struct __lklist pth_evlist;
};

struct __pthread_key {
  int ptk_key;
  void(*ptk_dtor)(void*);
};

/* Allocate new pthread. */
struct __pthread*
__create_thread_object();

void
__destroy_thread_object(struct __pthread*);

void
__set_thread_object(struct __pthread*);

/* Get the current thread. */
struct __pthread*
__pthread_self();


