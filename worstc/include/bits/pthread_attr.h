/* pthread_attr.h
   Purpose: the definition of a pthread_attr_t. */
#pragma once

#ifndef __BITS_PTHREAD_ATTR_H
#define __BITS_PTHREAD_ATTR_H 1

struct __pthread_flags {
  int pf_stackrsize; /*< Stack reserve size. */
  int pf_stackcsize; /*< Stack commit size. */
  int pf_stackgsize; /*< Stack guard size.  */
  
#define __PF_DETACHED_BIT (1 << 0) /*< Detached (set) or joinable (unset). */
  unsigned int pf_ctx; /*< Contextual Flags. */
  void* pf_stackaddr;
};

typedef struct __pthread_flags pthread_attr_t;

enum {
#define PTHREAD_MUTEX_DEFAULT PTHREAD_MUTEX_NORMAL

  PTHREAD_MUTEX_NORMAL = 0,
  PTHREAD_MUTEX_ERRORCHECK,
  PTHREAD_MUTEX_RECURSIVE,
  __MUTEXATTR_TYPE_MAX = 2,
  
  PTHREAD_MUTEX_STALLED = 0,
  PTHREAD_MUTEX_ROBUST,
  __MUTEXATTR_ROBUSTNESS_MAX = 1
};

struct __pthread_mutex_flags {
  short int mf_type;
  short int mf_robustness;
};

typedef struct __pthread_mutex_flags pthread_mutexattr_t;

#endif

