/* pthread_types.h
   Purpose: Types which pthread.h and sys/types.h define. */
#pragma once

#ifndef _BITS_PTHREAD_TYPES_H
#define _BITS_PTHREAD_TYPES_H 1

/* I don't remember if state bookkeeping
   is necessary for any of these types
   so make them just alias to int. */

typedef int pthread_barrier_t;
typedef int pthread_cond_t;
typedef void* pthread_key_t;
typedef int pthread_spinlock_t;

typedef void *pthread_t;

typedef unsigned long pthread_barrierattr_t;
typedef unsigned long pthread_condattr_t;

#endif
