/* setjmp.h
   Purpose: definition of private stuff for setjmp */
#pragma once
#ifndef __BITS_SETJMP_H
#define __BITS_SETJMP_H 1

#include "bits/config.h"

struct __jmp_data {
    unsigned long jd_gpr[15];
    unsigned long jd_rsp;
    unsigned long jd_rip;
};

typedef struct __jmp_data jmp_buf[1];

/* Fill all registers which get implicitly clobbered
   by the compiler. */
__WC_returns_twice
extern int __sj_fill_jmp(jmp_buf);

__WC_noreturn
extern int __sj_jmp(jmp_buf, int status);

#endif
