/* setjmp_asm.h
   Purpose: definitions used by setjmp asm and C
*/
#pragma once

#define SJ_AX 0
#define SJ_BX 1
#define SJ_CX 2
#define SJ_DX 3
#define SJ_DI 4
#define SJ_SI 5
#define SJ_BP 6
#define SJ_R8 7
#define SJ_R9 8
#define SJ_R10 9
#define SJ_R11 10
#define SJ_R12 11
#define SJ_R13 12
#define SJ_R14 13
#define SJ_R15 14