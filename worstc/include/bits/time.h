/* time.h
   Purpose: definition of time types. */
#pragma once

#ifndef __WC_BITS_TIME_H
#define __WC_BITS_TIME_H

#include "bits/config.h"

#define __need_size_t
#include <stddef.h>

#define __SECONDS_PER_DAY 86400

/* For NON-LEAP years. */
#define __SECONDS_PER_YEAR 31556926

__WC_hidden
int __cal_getdaycount(int mo, int ye);    

typedef unsigned long __time_t;

typedef unsigned long long __clock_t;

typedef unsigned int __clockid_t;

typedef unsigned long __timer_t;

#endif

