/* ctype.h
   Purpose: character handling */
#pragma once

#ifndef __CTYPE_H
#define __CTYPE_H 1

#include "bits/config.h"

__WC_EXTERN_C_BEGIN

/* Adapt these for locales! */

static inline
int islower(int _c)
{
    return (_c >= 'a' && _c <= 'z');
}


static inline
int isupper(int _c)
{
    return (_c >= 'A' && _c <= 'Z');
}

static inline
int isalpha(int _c)
{
    return islower(_c)
        || isupper(_c);
}

static inline
int isdigit(int _c)
{
    return _c >= '0' && _c <= '9';
}

static inline
int isalnum(int _c)
{
    return isalpha(_c) || isdigit(_c);
}

static inline
int isblank(int _c)
{
    return _c == ' ';
}
static inline
int iscntrl(int _c)
{
    return _c == '\n' || _c == '\r' || _c == '\e';
}

static inline
int ispunct(int _c)
{
    return (_c >= '!' && _c <= '/')
        || (_c >= ':' && _c <= '@')
        || (_c >= '[' && _c <= '`')
        || (_c >= '{' && _c <= '~');
}

static inline
int isspace(int _c)
{
    return _c == ' ';
}

static inline
int isxdigit(int _c)
{
    return isdigit(_c) 
        || (_c >= 'a' && _c <= 'f')
        || (_c >= 'A' && _c <= 'F');
}

static inline
int isgraph(int _c)
{
    return isalnum(_c) || ispunct(_c);
}

static inline
int isprint(int _c)
{
    return isgraph(_c) || isspace(_c);
}

static inline
int toupper(int _c)
{
    if (!islower(_c))
        return _c;

    return 'A' + (_c - 'a'); 
}

static inline
int tolower(int _c)
{
    if (!isupper(_c))
        return _c;

    return 'a' + (_c - 'A'); 
}

__WC_EXTERN_C_END

#endif

