/* errno.h
   Purpose: error signaling */
#pragma once

#ifndef __ERRNO_H
#define __ERRNO_H 1
#include "bits/config.h"

#define errno (*_get_errno())

#define E2BIG  1
#define EACCES 2
#define EADDRINUSE 3
#define EADDRNOTAVAIL 4
#define EAFNOSUPPORT  5
#define EAGAIN        6
#define EALREADY      7
#define EBADF         8
#define EBADMSG       9
#define EBUSY         10
#define ECANCELED     11
#define ECHILD        12
#define ECONNABORTED  13
#define ECONNREFUSED  14
#define ECONNRESET    15
#define EDEADLK       16
#define EDESTADDRREQ  17
#define EDOM          18
#define EDQUOT        19
#define EEXIST        20
#define EFAULT        21
#define EFBIG         22
#define EHOSTUNREACH  23
#define EIDRM         24
#define EILSEQ        25
#define EINPROGRESS   26
#define EINTR         27
#define EINVAL        28
#define EIO           29
#define EISCONN       30
#define EISDIR        31
#define ELOOP         32
#define EMFILE        33
#define EMSGSIZE      34
#define EMULTIHOP     35
#define ENAMETOOLONG  36
#define ENETDOWN      37
#define ENETRESET     38
#define ENETUNREACH   39
#define ENFILE        40
#define ENOBUFS       41
#define ENODATA       42
#define ENODEV        43
#define ENOENT        44
#define ENOEXEC       45
#define ENOLCK        46
#define ENOLINK       47
#define ENOMEM        48
#define ENOMSG        49
#define ENOPROTOOPT   50
#define ENOSPC        51
#define ENOSR         52
#define ENOSTR        53
#define ENOSYS        54
#define ENOTCONN      55
#define ENOTDIR       56
#define ENOTEMPTY     57
#define ENOTRECOVERABLE 58
#define ENOTSOCK        59
#define ENOTSUP         60
#define ENOTTY          61
#define ENXIO           62
#define EOPNOTSUPP      ENOTSUP
#define EOVERFLOW       63
#define EOWNERDEAD      64
#define EPERM           65
#define EPIPE           66
#define EPROTO          67
#define EPROTONOSUPPORT 68
#define EPROTOTYPE      69
#define ERANGE          70
#define EROFS           71
#define ESPIPE          72
#define ESRCH           73
#define ESTALE          74
#define ETIME           75
#define ETIMEDOUT       76
#define ETXTBUSY        77
#define EWOULDBLOCK     78
#define EXDEV           79
#define EMLINK          80

__WC_EXTERN_C_BEGIN

extern int*
_get_errno();

extern int
__ecode_into_errno(int ecode);

/** Required by fdlibm. */
void __set_errno(int error);

__WC_EXTERN_C_END

#endif

