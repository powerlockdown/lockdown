/* fcntl.h
   Purpose: file control. */
#pragma once

#ifndef __FCNTL_H
#define __FCNTL_H 1

#include "bits/config.h"
#include "sys/types.h"

/* Let's establish some simple logic here.
   
   Mutually exclusive flags live in the first 8 bits.
   Flags meant to be OR-ed one another live in the rest. */

enum { /*< File access modes. */
   O_EXEC   = (1 << 0),
   O_RDONLY = (1 << 1),
   O_RDWR   = (1 << 2),
   O_SEARCH = (1 << 3),
   O_WRONLY = (1 << 4)
};

enum { /*< Behavioural flags. */
   O_APPEND    = (1 << 8),
   O_CLOEXEC   = (1 << 9),
   O_CREAT     = (1 << 10),
   O_DIRECTORY = (1 << 11),
   O_DSYNC     = (1 << 12),
   O_EXCL      = (1 << 13),
   O_NOCTTY    = (1 << 14),
   O_NOFOLLOW  = (1 << 15),
   O_NONBLOCK  = (1 << 16),
   O_RSYNC     = (1 << 17),
   O_SYNC      = (1 << 18),
   O_TRUNC     = (1 << 19),
   O_TTY_INIT  = (1 << 20)
};

__WC_EXTERN_C_BEGIN

extern int
open(const char* _pth, int flags, ...);
/* openat. */

extern int
posix_openpt(int oflag);

/* possible values for _c. */
enum {
   F_DUPFD = 1,
   F_GETFD,
   F_SETFD,

   F_GETFL,
   F_SETFL,

   F_GETOWN,
   F_SETOWN,

   /* F_{G,S}ETLK and F_SETLKW. */
};

extern int
fcntl(int _fd, int _c, ...);

__WC_EXTERN_C_END

#endif
