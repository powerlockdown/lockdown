/* fenv.h
   Purpose: Floating point environment */
#pragma once

#ifndef __FENV_H
#define __FENV_H 1

#include "bits/config.h"
#include "bits/fenv.h"

__WC_EXTERN_C_BEGIN

/* Clear exceptions *exc* from FP status */
extern
int feclearexcept(int _exc);

extern
int feraiseexcept(int _exc);

extern int fegetround();
extern int fesetround(int _roundness);

extern int feholdexcept(fenv_t*);

extern int fesetenv(fenv_t*);
extern int fegetenv(fenv_t* env);

__WC_EXTERN_C_END

#endif

