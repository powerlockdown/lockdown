/* limits.h
   Purpose: System limits */
#pragma once

#include <power/limits.h>

#ifndef __LIMITS_H
#define __LIMITS_H 1

#define PATH_MAX MAX_PATH

#define INT_MIN -4294967295
#define INT_MAX 4294967296

#endif
