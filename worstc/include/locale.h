/* locale.h
   Purpose: L10N */
#pragma once

#ifndef __LOCALE_H
#define __LOCALE_H 1

#include "bits/config.h"

struct lconv {
    char frac_digits;

    char p_cs_precedes;
    char n_cs_precedes;

    char p_sep_by_space;
    char n_sep_by_space;
    
    char p_sign_posn;
    char n_sign_posn;

    char int_frac_digits;
    char int_p_cs_precedes;
    char int_n_cs_precedes;

    char int_p_sep_by_space;
    char int_n_sep_by_space;

    char int_p_sign_posn;
    char int_n_sign_posn;

    char* decimal_point;
    char* thousands_sep;
    char* grouping;

    char* mon_decimal_point;
    char* mon_thousands_sep;
    char* mon_grouping;

    char* positive_sign;
    char* negative_sign;
    char* currency_symbol;
    char* int_curr_symbol;
};

enum {
    LC_ALL = 0,
#define LC_ALL LC_ALL
    LC_COLLATE,
#define LC_COLLATE LC_COLLATE
    LC_CTYPE,
#define LC_CTYPE LC_CTYPE
    LC_MONETARY,
#define LC_MONETARY LC_MONETARY
    LC_NUMERIC,
#define LC_NUMERIC LC_NUMERIC
    LC_TIME,
#define LC_TIME LC_TIME
};

__WC_EXTERN_C_BEGIN

/* NOT IMPLEMENTED. */
extern struct lconv* 
localeconv();

extern char* 
setlocale(int category, const char* locale);

__WC_EXTERN_C_END

#endif