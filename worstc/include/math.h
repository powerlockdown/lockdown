/* math.h
   Purpose: Mathematics. */
#pragma once

#include "bits/config.h"
#include "bits/i754.h"

#ifndef __MATH_H
#define __MATH_H 1

#define _MATH_H 1 /* fdlibm will basically recreate
                      this header with its own values 
		      ifndeffed.*/
#define _MATH_H_ 1

#ifndef __MATH_STATIC
#define __MATH_extern extern
#else
#define __MATH_extern __attribute__((noplt))
#endif

#include "bits/math_consts.h"

#define FP_INFINITE  (1 << 0)
#define FP_NAN       (1 << 1)
#define FP_NORMAL    (1 << 2)
#define FP_SUBNORMAL (1 << 3)
#define FP_ZERO      (1 << 4)

__WC_EXTERN_C_BEGIN

#define issignalingf __issignalingf
#define issignalingl __issignalingl

extern int __issignalingf(float _f);
extern int __issignalingl(double _l);

extern int __fpclassifyf32(float _f);
extern int __fpclassifyf64(double _f);

__WC_EXTERN_C_END

#define issignaling(X) \
  sizeof(X) == sizeof(float) ? __issignalingf(X) : __issignalingl(X)

#define fpclassify(X) \
    sizeof(X) == sizeof(float) ? __fpclassifyf32(X) : __fpclassifyf64(X)

#define isfinite(X)    (fpclassify(X) & ~(FP_NAN | FP_INFINITE))
#define isnormal(X)    (fpclassify(X) == FP_NORMAL)
#define isnan(X)       (fpclassify(X) == FP_NAN)
#define isinf(X)       (fpclassify(X) == FP_INFINITE)
#define isunordered(X, y) (isnan(X) || isnan(y))

#if defined(_HAS_C99)

#define HUGE_VAL ((double)1e12000)
#define HUGE_VALF 12e12000f
#define HUGE_VALL HUGE_VAL

#if __WC_GNU_VERSION >= 303
#define NAN __builtin_nanf("")
#define INFINITY __builtin_inff()

#undef HUGE_VAL
#undef HUGE_VALF
#undef HUGE_VALL

#define HUGE_VAL __builtin_inf()
#define HUGE_VALF INFINITY
#define HUGE_VALL __builtin_infl()

#else
#define NAN (0.0f / 0.0f)
#define INFINITY HUGE_VALF
#endif

#endif

#define __copysign(X, y) copysign(X, y)

inline int __extract_signbit_32(float _x)
{
    union __i754_float32 __flt;
    __flt.Value = _x;
    return __flt.Raw & (1 << 31);
}

inline int __extract_signbit_64(double _x)
{
    union __i754_float64 __flt;
    __flt.Value = _x;
    return (__flt.Raw & ((unsigned long)1 << 63)) >> 63;
}

#ifdef __STDC_WANT_IEC_60559_BFP_EXT__
/* TODO: issignaling */
#define issubnormal(X) (fpclassify(X) == FP_SUBNORMAL)
#define iszero(X)      (fpclassify(X) == FP_ZERO)
#endif /* defined(WANT FP TS)*/

#if __has_builtin(__builtin_signbit)
#define signbit(X) __builtin_signbit(X)
#else
#define signbit(X) sizeof(X) == sizeof(float) \
    ? __extract_signbit_32(X) \
    : __extract_signbit_64(X)
#endif /* __has_builtin(__builtin_signbit) */

#define __signbit(X) signbit(X)
#define __signbitl(X) signbit((long double)(X))
#define __signbitf(X) signbit((float)(X))

__WC_EXTERN_C_BEGIN

/* Formula-based functions
   acos - Compute the arc cosine of X. */
__MATH_extern double acos(double _x);
__MATH_extern float acosf(float _x);
__MATH_extern long double acosl(long double _x);

/* asin - Compute the arc sine of X. */
__MATH_extern double asin(double _x);
__MATH_extern float asinf(float _x);
__MATH_extern long double asinl(long double _x);

/* fabs - Compute the absolute value of X. */
__MATH_extern double fabs(double _x);
__MATH_extern float fabsf(float _x);
__MATH_extern long double fabsl(long double _x);

/* sqrt - Compute the square root of X. */
__MATH_extern float sqrtf(float _x);
__MATH_extern double sqrt(double _x);
extern long double sqrtl(long double _x);

/* log - Compute the natural logarithm of X.  */
__MATH_extern float logf(float _x);
__MATH_extern double log(double _x);

/* pow - Compute the X to the power of Y. */
__MATH_extern double pow(double _x, double _y);

/* copysign - Create fp from separate magnitude + signal. */
__MATH_extern double copysign(double _x, double _y);
__MATH_extern float copysignf(float _x, float _y);
extern long double copysignl(long double, long double);

/* NOT IMPLEMENTED */
__MATH_extern double atan(double);
__MATH_extern double atan2(double _y, double _x);

__MATH_extern double ceil(double);

__MATH_extern double cos(double x);
__MATH_extern double cosh(double x);

__MATH_extern double exp(double x);
__MATH_extern double floor(double x);

__MATH_extern double fmod(double x, double y);
__MATH_extern double frexp(double x, int *exp);
__MATH_extern double ldexp(double x, int exp);

__MATH_extern double log10(double x);
__MATH_extern double modf(double x, double *iptr);

__MATH_extern double sin(double x);
__MATH_extern double sinh(double x);

__MATH_extern double tan(double x);
__MATH_extern double tanh(double x);

__MATH_extern float expm1f(float x);

__MATH_extern double gamma_r(double x, int*);
__MATH_extern float gammaf_r(float x, int *);
__MATH_extern long double gammal_r(long double x, int *);

__MATH_extern double lgamma_r(double x, int*);
__MATH_extern float lgammaf_r(float x, int *);
__MATH_extern long double lgammal_r(long double x, int *);

__MATH_extern double trunc(double x);
__MATH_extern float truncf(float x);
__MATH_extern long double truncl(long double x);

__MATH_extern double log2(double x);
__MATH_extern float log2f(float x);
__MATH_extern long double log2l(long double);

__MATH_extern double cbrt(double x);
__MATH_extern float cbrtf(float x);
__MATH_extern long double cbrtl(long double x);

__WC_EXTERN_C_END

#endif
