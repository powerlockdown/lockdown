/* pthread.h
   Purpose: POSIX thread interface. */
#pragma once

#ifndef _PTHREAD_H
#define _PTHREAD_H 1

#include "bits/pthread_attr.h"
#include "bits/config.h"
#include "bits/pthread_types.h"
#include "bits/mutex.h"

__WC_EXTERN_C_BEGIN

#define PTHREAD_MUTEX_INITIALIZER _PMTX_INITIALIZER

extern int pthread_key_create(pthread_key_t* pk,
			      void (*dtor)(void*));
extern int pthread_key_delete(pthread_key_t pk);

extern void* pthread_getspecific(pthread_key_t key);
extern int pthread_setspecific(pthread_key_t key, const void *value);

extern int pthread_create(pthread_t* __WC_restrict,
			  const pthread_attr_t* __WC_restrict,
			  void* (*)(void*), void*);
extern void pthread_exit(void*);

extern int pthread_mutex_init(pthread_mutex_t* __WC_restrict mutex,
                              const pthread_mutexattr_t *restrict attr);
extern int pthread_mutex_destroy(pthread_mutex_t*);
extern int pthread_mutex_lock(pthread_mutex_t* mutex);    
extern int pthread_mutex_unlock(pthread_mutex_t* mutex);

extern int pthread_attr_init(pthread_attr_t *);
extern int pthread_attr_destroy(pthread_attr_t *);

extern int pthread_attr_setstackaddr(pthread_attr_t* attr, void* stk);
extern int pthread_attr_setstacksize(pthread_attr_t* attr, size_t length);
extern int pthread_attr_setstack(pthread_attr_t* attr, void* addr,
                                 size_t length);

extern int pthread_mutexattr_init(pthread_mutexattr_t* mat);
extern int pthread_mutexattr_destroy(pthread_mutexattr_t *);

extern int pthread_mutexattr_settype(pthread_mutexattr_t*, int type);
extern int pthread_mutexattr_setrobust(pthread_mutexattr_t*, int rob);

extern int pthread_rwlock_rdlock(pthread_rwlock_t* rwl);
extern int pthread_rwlock_wrlock(pthread_rwlock_t* rwl);
extern int pthread_rwlock_unlock(pthread_rwlock_t* rwl);

__WC_EXTERN_C_END

#endif
