/* setjmp.h
   Purpose: perform non-local gotos */
#pragma once

#ifndef __SETJMP_H
#define __SETJMP_H 1

#include "bits/config.h"
#include "bits/setjmp.h"

__WC_EXTERN_C_BEGIN

#define setjmp __sj_fill_jmp

#ifdef __cplusplus
/* libstdc++ wants a function rather than a macro. */
__WC_noreturn inline int longjmp(jmp_buf _buffer, int _status)
{
   __sj_jmp(_buffer, _status);
}

#else
#define longjmp __sj_jmp
#endif

__WC_EXTERN_C_END

#endif
