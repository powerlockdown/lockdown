/* signal.h
   Purpose: Asynchronous software notification */
#pragma once

#ifndef __SIGNAL_H
#define __SIGNAL_H 1

#define __stypes_reduced
#define __want_pid
#define __want_gid
#include "sys/types.h"

#include <power/signal.h>

enum {
#define SIG_BLOCK SIGBLOCK
  SIG_BLOCK,
#define SIG_UNBLOCK SIG_UNBLOCK
  SIG_UNBLOCK,
#define SIG_SETMASK SIG_SETMASK
  SIG_SETMASK
};

#define SIG_ERR ((void*)0)
#define SIG_DFL ((void*)0x1)
#define SIG_HOLD ((void*)0x2)
#define SIG_IGN  ((void*)0x3)

typedef int sig_atomic_t;

union sigval {
    int   sival_int;
    void* sival_ptr;
};

enum {
    SIGEV_NONE = 0,
    SIGEV_SIGNAL,
    SIGEV_THREAD
};

struct sigevent {
    int sigev_notify;
    int sigev_signo;
    union sigval sigev_value;
};

typedef struct {
    int si_signo;
    int si_code;

    int     si_errno;
    __pid_t si_pid;
    __pid_t si_uid;
    void*   si_addr;

    int  si_status;
    long si_band;
} siginfo_t;

typedef unsigned long sigset_t;

typedef void (*__signal_handler)(int);

extern __signal_handler signal(int, __signal_handler);

extern int raise(int);

extern int sigwait(sigset_t* __WC_restrict, int* __WC_restrict);
extern int sigprocmask(int how, const sigset_t* __WC_restrict set,
                       sigset_t* __WC_restrict oset);

extern int sigemptyset(sigset_t*);
extern int sigfillset(sigset_t*);

extern int sigaddset(sigset_t* set, int sig);
extern int sigdelset(sigset_t* set, int sig);

extern int kill(pid_t pid, int sig);

#undef __stypes_reduced

#endif
