/* stdint.h
   Purpose: standard integer types */
#pragma once

#ifndef __STDINT_H
#define __STDINT_H 1

/* Signed types. */
typedef signed char  int8_t;
typedef signed short int16_t;
typedef signed int   int32_t;
typedef signed long  int64_t;

/* Unsigned types. */
typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;

/* Minimum-sized types. */
typedef int32_t int_least8_t;
typedef int32_t int_least16_t;
typedef int64_t int_least32_t;
typedef int64_t int_least64_t;

typedef uint32_t uint_least8_t;
typedef uint32_t uint_least16_t;
typedef uint64_t uint_least32_t;
typedef uint64_t uint_least64_t;

/* Fastest types. */
typedef uint32_t uint_fast8_t;
typedef uint32_t uint_fast16_t;
typedef uint64_t uint_fast32_t;
typedef uint64_t uint_fast64_t;

/* Misc. types */
typedef unsigned long int uintptr_t;
typedef signed long int intptr_t;

typedef unsigned long long uintmax_t;
typedef signed long long intmax_t;

/* Limits */
#define __INT8_VAL 128
#define __UINT8_VAL 255

#define INT8_MIN -(__INT8_VAL)
#define INT8_MAX (INT8_VAL - 1)

#define __INT16_VAL 32768
#define __UINT16_VAL 65535

#define INT16_MIN -(__INT16_VAL)
#define INT16_MAX (__INT16_VAL - 1)

#define __INT32_VAL 2147483648
#define __UINT32_VAL 4294967295

#define INT32_MIN -(__INT32_VAL)
#define INT32_MAX (__INT32_VAL - 1)

#define __INT64_VAL 9223372036854775808LL
#define __UINT64_VAL 18446744073709551615LL

#define INT64_MIN -(__INT64_VAL)
#define INT64_MAX (__INT64_VAL - 1)

#define UINT8_MAX __UINT8_VAL
#define UINT16_MAX __UINT16_VAL
#define UINT32_MAX __UINT32_VAL
#define UINT64_MAX __UINT64_VAL

#define INT_LEAST8_MIN INT32_MIN
#define INT_LEAST8_MAX INT32_MAX

#define INT_LEAST16_MIN INT32_MIN
#define INT_LEAST16_MAX INT32_MAX

#define INT_LEAST32_MIN INT64_MIN
#define INT_LEAST32_MAX INT64_MAX

#define INT_LEAST64_MIN INT64_MIN
#define INT_LEAST64_MAX INT64_MAX

#define INTPTR_MIN INT64_MIN
#define INTPTR_MAX INT64_MAX
#define UINTPTR_MAX UINT64_MAX

#define INTMAX_MIN INT64_MIN
#define INTMAX_MAX INT64_MAX
#define UINTAX_MAX UINT64_MAX

#define PTRDIFF_MIN INT64_MIN
#define PTRDIFF_MAX INT64_MAX

#define SIG_ATOMIC_MIN INT32_MIN
#define SIG_ATOMIC_MAX INT32_MAX

#define LONG_LONG_MIN INT64_MIN
#define LONG_LONG_MAX INT64_MAX

#endif
