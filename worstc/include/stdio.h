/* stdio.h
   Purpose: Facilities for I/O. */
#pragma once

#ifndef __STDIO_H
#define __STDIO_H 1

#include "bits/config.h"
#include "bits/FILE.h"

#define __need_size_t
#include <stddef.h>

#include <stdarg.h>

#define EOF -2

/* The limit enforced by the kernel is 64 but
   we cannot trust the user will only use handles
   behind fopen. (Why would we? Not every object
   can be read/written from.)  */
#define FOPEN_MAX 32

/* libstdc++ uses these values for their ios_base
   and expect _CUR and _END to NOT BE ZERO!

   They also expect none of the two to be 65536
   but that's irrelevant for me. */
#define SEEK_CUR 1
#define SEEK_SET 0
#define SEEK_END 2

#define _IONBF 0 /*< Unbuffered. */
#define _IOFBF 1 /*< Fully buffered. */ 
#define _IOLBF 2 /*< Line buffered. */

#define BUFSIZ 128

typedef struct _FILE_struct FILE;

__WC_EXTERN_C_BEGIN

extern FILE __stdout;
extern FILE __stdin;
extern FILE __stderr;

#define stdout (&__stdout)
#define stderr (&__stderr)
#define stdin (&__stdin)

typedef struct {
      /* TODO: FILL THIS STRUCT WHEN fgetpos AND fsetpos
         GETS IMPLEMENTED */
} fpos_t;

/* Open handle for file located at name. */
extern FILE* fopen(const char* __WC_restrict _name, 
                   const char* __WC_restrict _mode);

/* Close handle stream. */
extern int fclose(FILE* _stream);

/* Push character onto stream's internal buffer */
extern int ungetc(int _c, FILE* _stream);

/* Extract character from stream's internal buffer */
extern int fgetc(FILE* _stream);

/* Read size elements from stream. */
extern size_t fread(void* __WC_restrict _buffer, 
                    size_t _size, 
                    size_t _memberSize, 
                    FILE* _f);

extern size_t 
fwrite(const void* __WC_restrict buffer, size_t size, size_t count, FILE* __WC_restrict stream);

/* Jump to offset relative to stream's base. */
extern int fseek(FILE* _stream, 
                 long int _off, 
                 int _base);

/* Get current file position */
extern long int ftell(FILE* _stream);

/* Remove error status from handle */
extern void clearerr(FILE* _stream);

/* Is there chars left to be read? */
extern int feof(FILE* _stream);

/* Did something went wrong during read/write? */
extern int ferror(FILE* _stream);

/* Write char to the stdout */
extern int putchar(int _c);

/* Write a char into the buffer */
extern int fputc(int _c, 
                 FILE* _stream);

/* Write string S to the buffer. */
extern int fputs(const char* __WC_restrict _s,
                 FILE* _stream);

/* Flush the stream. */
extern int fflush(FILE* _stream);

/* Resets the stream seek offset. */
extern void rewind(FILE* _stream);

/* Associate a new file with the specified stream. */
extern FILE*
freopen(const char* __WC_restrict _filename, const char* __WC_restrict _mode,
        FILE* __WC_restrict _stream);

/* Set internal bfr buffer. */
extern int
setvbuf(FILE* __WC_restrict _stream, char* __WC_restrict _buffer,
        int _mode, size_t _size);
extern void
setbuf(FILE* __WC_restrict _stram, char* __WC_restrict _buffer);

/* Formatted print functions */

/* Write formatted string `msg` to stream `stream`. */
extern int 
fprintf(FILE* _stream, const char* _msg, ...);
extern int 
vfprintf(FILE* _stream, const char* _msg, va_list _args);

/* Write formatted string `msg` to stdout or to a buffer. */
extern int 
printf(const char* __WC_restrict _ft, ...);
extern int 
sprintf(char* __WC_restrict _bf, const char* __WC_restrict _ft, ...);
extern int 
snprintf(char* __WC_restrict _bf, size_t _nb, const char* __WC_restrict _ft, ...);

extern int 
vprintf(const char* __WC_restrict _ft, va_list _v);
extern int 
vsprintf(char* __WC_restrict _bf, const char* __WC_restrict _ft, va_list _v);
extern int
vsnprintf(char* __WC_restrict _bf, size_t _bs, const char* _ft, va_list _v);

/* POSIX API. */
extern int
fileno(FILE* _stream);
extern FILE*
fdopen(int _fd, const char* mode);

/* NOT IMPLEMENTED. */
extern int
remove(const char* _file);
extern int
rename(const char* _oldn, const char* _newn);

extern FILE* 
tmpfile();
extern char*
tmpnam(char*);

extern int
fgetpos(FILE* __WC_restrict _stream, fpos_t* __WC_restrict _fps);
extern int
fsetpos(FILE* __WC_restrict _stream, const fpos_t* _fps);

int 
vfscanf(FILE* __WC_restrict stream, const char* __WC_restrict format, va_list va);

extern int
scanf(const char* __WC_restrict format, ...);
extern int 
fscanf(FILE* __WC_restrict _stream, const char* __WC_restrict format, ...);
extern int 
sscanf(const char* __WC_restrict _buffer, const char* __WC_restrict format, ...);

extern int 
vsscanf(const char* __WC_restrict bf, const char* __WC_restrict format, va_list va);

extern int
puts(const char* _str);

extern void
perror(const char*);
extern
int getchar();

extern char*
fgets(char* __WC_restrict str, int count, FILE* __WC_restrict stream);

static inline 
int putc(int _c, FILE* _s)
{
    return fputc(_c, _s);
}

static inline 
int getc(FILE* _s)
{
    return fgetc(_s);
}

__WC_EXTERN_C_END

#endif
