/* stdlib.h
   Purpose: General utility routines */
#pragma once

#include "bits/config.h"
#include <stddef.h>

#ifndef __STDLIB_H
#define __STDLIB_H 1

#include "bits/div.h"
#include "bits/atexit.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE -1

__WC_EXTERN_C_BEGIN

/* Communicating the environment that
   you want to die. */
extern __noret 
void abort(void);
extern __noret
void exit(int _code);

/* Manipulating memory at a granularity
   smaller than a page. */
extern void* calloc(size_t _arraySize, size_t _arrayItemSize);
extern void free(void* _ptr);
extern void* malloc(size_t _size);
extern void* realloc(void* _ptr, size_t _size);

/* Converting signed string numbers 
   to real integers  */
extern long strtol(const char* __WC_restrict _string, char** __WC_restrict _endptr, int _bs);
extern long long strtoll(const char* __WC_restrict _string, char** __WC_restrict _endptr, int _bs);

/* Converting unsigned string numbers 
   to real integers  */
extern unsigned long long strtoull(const char* __WC_restrict _string, char** __WC_restrict _endptr, int _bs);
extern unsigned long int strtoul(const char* __WC_restrict _string, char** __WC_restrict _endptr, int _bs);

/* Converting decimal point numbers
   to floating point.  */
extern double strtod(const char* __WC_restrict _string, char** __WC_restrict);
extern float strtof(const char* __WC_restrict _string, char** __WC_restrict);
extern long double strtold(const char* __WC_restrict _string, char** __WC_restrict);

/* Take the absolute value of val. */
extern int abs(int _val);
extern long labs(long _val);
extern long long llabs(long long _val);

/* Convert string numbers to integers/floating point.s */
extern double atof(char*);

extern int atoi(char*);
extern long atol(char*);
extern long long atoll(char*);

/* Get slave name of tty. */
extern char* ptsname(int tty);

/* NOT IMPLEMENTED. */
extern int
system(const char*);

extern void* 
bsearch(const void* _key, const void* _base,
        size_t _nm, size_t _size,
        int (*_compar)(const void *, const void *));
extern void
qsort(void* _base, size_t _nmemb, size_t _size,
      int (*_compar)(const void *, const void *));

extern char*
getenv(const char*);

extern int
rand();
extern void
srand(int);

__WC_EXTERN_C_END

#endif
