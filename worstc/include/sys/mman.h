/* mman.h
   Purpose: Memory MANager. */
#pragma once

#ifndef __SYS_MMAN_H
#define __SYS_MMAN_H 1

#define __need_size_t
#include <stddef.h>

#include "bits/config.h"

#define __stypes_reduced
#define __want_off
#include "types.h"

/* Mapping privacy... */
#define MAP_SHARED  0
#define MAP_PRIVATE (1 << 0) 

#define MAP_FIXED (1 << 1)
#define MAP_ANONYMOUS (1 << 2)

/* Mapping protection... */
#define PROT_NONE 0

#define PROT_WRITE (1 << 1)
#define PROT_READ  (1 << 2)
#define PROT_EXEC  (1 << 3)

/* Make the address non-canonical so
   the user will crash if it tries to
   inadvertly read it. */
#define MAP_FAILED ((void*)(-1ULL << 2))

__WC_EXTERN_C_BEGIN

extern void*
mmap(void* _addr, size_t _length, int _prot, int _flags, 
     int _fd, __off_t _off);

extern int
munmap(void* _addr, size_t _length);

__WC_EXTERN_C_END

#endif
