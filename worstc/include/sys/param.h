/* param.h
   Purpose: old school unix limit */
#pragma once

#ifndef __SYS_PARAM_H
#define __SYS_PARAM_H 1

#define NBBY 8 /*< Number of Bits in a BYte (char, actually). */

#endif
