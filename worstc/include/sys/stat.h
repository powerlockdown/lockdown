/* stat.h
   Purpose: file statistics */
#pragma once

#ifndef __SYS_STAT_H
#define __SYS_STAT_H 1

#include "bits/config.h"
#include "bits/time.h"

#include "types.h"

struct stat {
   dev_t st_dev;
   ino_t st_ino;
   mode_t st_mode;
   nlink_t st_nlink;

   uid_t st_uid;
   gid_t st_gid;

   dev_t st_rdev;
   off_t st_size;

   __time_t st_atime;
   __time_t st_mtime;
   __time_t st_ctime;

   blksize_t st_blksize;
   blkcnt_t st_blocks;
};

#define S_IRWXU 0700
#define S_IRUSR 0400
#define S_IWUSR 0200
#define S_IXUSR 0100

#define S_IRWXG 070
#define S_IRGRP 040
#define S_IWGRP 020
#define S_IXGRP 010

#define S_IRWXO 07
#define S_IROTH 04
#define S_IWOTH 02
#define S_IXOTH 01

#define S_ISUID 04000
#define S_ISGID 02000
#define S_ISVTX 01000

enum {
#define S_IFBLK S_IFBLK
  S_IFBLK,
#define S_IFCHR S_IFCHR
  S_IFCHR,
#define S_IFIFO S_IFIFO
  S_IFIFO,
#define S_IFREG S_IFREG
  S_IFREG,
#define S_IFDIR S_IFDIR
  S_IFDIR,
#define S_IFLNK S_IFLNK
  S_IFLNK,
#define S_IFSOCK S_IFSOCK
  S_IFSOCK
};

#define S_ISBLK(X) ((X) == S_IFBLK)
#define S_ISCHR(X) ((X) == S_IFCHR)
#define S_ISFIFO(X) ((X) == S_IFIFO)
#define S_ISREG(X) ((X) == S_IFREG)
#define S_ISDIR(X) ((X) == S_IFDIR)
#define S_ISLNK(X) ((X) == S_IFLNK)
#define S_ISSOCK(X) ((X) == S_IFSOCK)

__WC_EXTERN_C_BEGIN

extern int
stat(const char* __WC_restrict _path, struct stat* __WC_restrict _st);

extern int
fstat(int, struct stat* /* Not declared restrict? */);

__WC_EXTERN_C_END

#endif
