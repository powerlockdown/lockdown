/* types.h
   Purpose: data types */
#pragma once

#include "bits/time.h"

#ifndef __stypes_reduced
#endif

#define __types_need(T) !defined(__stypes_reduced)  \
      || (defined(__stypes_reduced) && defined(__want_ ## T))

#if __types_need(blkcnt)
typedef unsigned long __blkcnt_t;
#endif

#if __types_need(blksize)
typedef unsigned long __blksize_t;
#endif

#if __types_need(fsblkcnt)
typedef unsigned long long __fsblkcnt_t;
#endif

#if __types_need(fsfilcnt)
typedef unsigned long __fsfilcnt_t;
#endif

#if __types_need(ino)
typedef unsigned long __ino_t;
#endif

#if __types_need(mode)
typedef unsigned int __mode_t;
#endif

#if __types_need(nlink)
typedef unsigned long __nlink_t;
#endif

#if __types_need(off)
typedef signed long long __off_t;
#endif

#if __types_need(clock)
typedef __clock_t __sys_clock_t;
#endif

#if __types_need(clockid)
typedef __clockid_t __sys_clockid_t;
#endif

#if __types_need(dev)
typedef int __dev_t;
#endif

#if __types_need(gid)
typedef int __gid_t;
#endif

#if __types_need(id)
typedef long __id_t;
#endif

#if __types_need(key)
typedef unsigned int __key_t;
#endif

#if __types_need(pid)
typedef unsigned int __pid_t;
#endif

#ifndef __stypes_reduced

typedef __blkcnt_t blkcnt_t;
typedef __blksize_t blksize_t;
typedef __fsblkcnt_t fsblkcnt_t;
typedef __fsfilcnt_t fsfilcnt_t;

typedef __ino_t ino_t;
typedef __mode_t mode_t;
typedef __nlink_t nlink_t;
typedef __off_t off_t;

typedef __sys_clock_t clock_t;
typedef __sys_clockid_t clockid_t;

typedef __dev_t dev_t;
typedef __gid_t gid_t;
typedef __gid_t uid_t;
typedef __id_t id_t;

typedef __key_t key_t;
typedef __pid_t pid_t;

typedef signed long ssize_t;

#else

#undef __stypes_reduced

#endif

typedef signed long ssize_t;
typedef unsigned int pid_t;
