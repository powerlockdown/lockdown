/* termios.h
   Purpose: terminal input/output */
#pragma once

#include <sys/types.h>
#ifndef __TERMIOS_H
#define __TERMIOS_H 1

#include "bits/config.h"
#include <power/pty.h>

typedef int cc_t;
typedef int tcflag_t;
typedef int speed_t;

#define NCCS PTY_CC_LENGTH

#define ECHO  PTY_LECHO
#define ECHOE PTY_LECHOERS
#define ECHOK PTY_LECHOK
#define ECHONL PTY_LECHONL
#define ICANON PTY_LICANON
#define ISIG PTY_LISIG
#define NOFLSH PTY_NOFLSH
#define TOSTOP PTY_TOSTOP
#define IEXTEN PTY_LIEXTEN

#define VMIN PTY_VMIN
#define VTIME PTY_VTIME
#define VERASE PTY_VERASE
#define VINTR PTY_VINTR
#define VKILL PTY_VKILL
#define VQUIT PTY_VQUIT
#define VSTART PTY_VSTART
#define VSTOP PTY_VSTOP
#define VSUSP PTY_VSUSP

#define VEOF VMIN
#define VEOL VTIME

enum {
  B0,
  B50,
  B75,
  B110,
  B134,
  B150,
  B200,
  B300,
  B600,
  B1200,
  B1800,
  B2400,
  B4800,
  B9600,
  B19200,
  B38400,
};

#define ICRNL PTY_ICRTONL
#define IGNBRK PTY_IGNBRK
#define IGNCR PTY_IGNCR
#define INLCR PTY_INLCR
#define INPCK PTY_INPCK
#define ISTRIP PTY_ISTRIP
#define IXANY PTY_IXANY
#define IXOFF PTY_IXOFF
#define IXON PTY_IXON
#define BRKINT PTY_BRKINT
#define PARMRK PTY_PARMRK
#define IGNPAR PTY_IGNPAR

#define OPOST PTY_OPOST
#define ONLCR PTY_ONLCR
#define OCRNL PTY_OCRNL
#define ONOCR PTY_ONOCR
#define ONLRET PTY_ONLRET
#define OFDEL PTY_OFDEL
#define OFILL PTY_OFILL

#define CSIZE PTY_CSIZE
#define CS5 PTY_CS5
#define CS6 PTY_CS6
#define CS7 PTY_CS7
#define CS8 PTY_CS8

#define CSTOPB PTY_CSTOPB
#define CREAD PTY_CREAD
#define PARENB PTY_PARENB
#define PARODD PTY_PARODD
#define HUPCL PTY_HUPCL
#define CLOCAL PTY_CLOCAL

__WC_EXTERN_C_BEGIN

struct termios {
  tcflag_t c_iflag;
  tcflag_t c_oflag;
  tcflag_t c_cflag;
  tcflag_t c_lflag;
  cc_t c_cc[NCCS];
};

/* Ignored. tcsetattr always sets immediately. */
#define TCSANOW   0
#define TCSADRAIN 0
#define TCSAFLUSH 0

extern speed_t cfgetispeed(const struct termios*);
extern speed_t cfgetospeed(const struct termios*);
extern int cfsetispeed(struct termios*);
extern int cfsetospeed(struct termios*);

extern int tcdrain(int tty);
extern int tcflow(int tty, int);

#define TCIFLUSH 0
#define TCOFLUSH 1
#define TCIOFLUSH 2

extern int tcflush(int tty, int);
extern int tcgetattr(int, struct termios*);

extern __pid_t tcgetsid(int);
extern int tcsendbreak(int, int);
extern int tcsetattr(int, int, const struct termios*);

__WC_EXTERN_C_END

#endif
