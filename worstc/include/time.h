/* time.h
   Purpose: routines that tells the time */
#pragma once

#ifndef __TIME_H
#define __TIME_H 1

#include "bits/config.h"
#include "bits/time.h"

typedef __time_t time_t;
typedef __clock_t clock_t;

struct tm {
   int tm_sec;
   int tm_min;
   int tm_hour;

   int tm_mday;
   int tm_mon;
   int tm_year;

   int tm_wday; /*< Sunday = 0. */
   int tm_yday;
   int tm_isdst;
};

struct timespec {
    __time_t tv_sec;
    __time_t tv_nsec;
};

struct itimerspec {
    struct timespec it_interval;
    struct timespec it_value;
};

__WC_EXTERN_C_BEGIN

extern time_t
time(time_t*);

/* NOT IMPLEMENTED. */
extern double
difftime(time_t _t1, time_t _t2);

extern time_t
mktime(struct tm*);

extern clock_t
clock();

extern char*
asctime(const struct tm*);
/* _s. */

extern char*
ctime(const time_t*);
/* _s. */

extern size_t
strftime(char* __WC_restrict _str, size_t _ctb, const char* __WC_restrict _format,
         const struct tm* __WC_restrict _tm);

extern struct tm*
gmtime(const time_t*);
/* _r, _s. */

extern struct tm*
localtime(const time_t*);
/* _r, _s. */

extern
double difftime(time_t t1, time_t t0);

__WC_EXTERN_C_END

#endif
