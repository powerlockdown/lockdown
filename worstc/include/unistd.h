/* unistd.h
   Purpose: */
#pragma once

#ifndef __UNISTD_H
#define __UNISTD_H 1

#define __stypes_reduced
#define __want_off
#define __want_pid
#include "sys/types.h"
#include "bits/config.h"

__WC_EXTERN_C_BEGIN

extern int close(int _fd);

extern int write(int _fd, const void* _buffer, int _length);
extern int read(int _fd, void* _buffer, int _length);

extern char* getcwd(char* buffer, size_t length);
extern int chdir(const char* path);

extern void _exit(int status) __WC_noreturn;

extern int pipe(int fds[2]);
extern int isatty(int fd);

extern int sleep(unsigned sec);

#define R_OK (1)
#define W_OK (1 << 1)
#define X_OK (1 << 2)
#define F_OK (1 << 3)

extern int access(const char* path, int amode);

extern int dup(int fd);
extern int dup2(int fd1, int fd2);

/* NOT IMPLEMENTED */
extern __pid_t
getpid();

extern unsigned long
lseek(int _fd, unsigned long _offset, int _whence);


__WC_EXTERN_C_END

#endif
