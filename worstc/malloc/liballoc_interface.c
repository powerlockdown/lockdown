/* liballoc_interface.c
   Purpose: Interface used by liballoc */
#include "bits/mutex.h"
#include "liballoc.h"
#include "lockdown.h"
#include "power/system.h"

#include <stdint.h>
#include "pthread.h"
#include "bits/config.h"

/* There is a chicken-and-egg problem when
   the caller is one of pthread init functions. */
__WC_hidden
int main_called = 0;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int liballoc_lock()
{
  if (__WC_likely(main_called))
    return pthread_mutex_lock(&mutex);
  return 0;
}

int liballoc_unlock()
{
  if (__WC_likely(main_called))
    return pthread_mutex_unlock(&mutex);
  return 0;
}

void* liballoc_alloc(size_t pages)
{
  unsigned int pa = pages * 4096;
  return VirtualMap(0, pa, VMAP_ANONYMOUS | VPROT_WRITE);
}

int liballoc_free(void* p, size_t length)
{
  VirtualUnmap((uint64_t)p, length * 4096);
  return 0;
}
