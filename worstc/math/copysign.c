/* copysign.c
   Purpose: the implementation of copysign. */
#include "math.h"
#include "bits/i754.h"

double copysign(double x, double y)
{
    union __i754_float64 cx;
    union __i754_float64 cy;

    cx.Value = x;
    cy.Value = y;

    cx.Raw &= ~(1ULL << 63);
    cx.Raw |= cy.Raw & (1ULL << 63);
    return cx.Value;
}

float copysignf(float x, float y)
{
  union __i754_float32 cx;
  union __i754_float32 cy;

  cx.Value = x;
  cy.Value = y;

  cx.Raw &= ~(1 << 31);
  cx.Raw |= cy.Raw & (1 << 31);
  return cx.Value;
}

long double
copysignl(long double x, long double y)
{
  union __fpu_ldbl cx, cy;
  cx.Value = x;
  cy.Value = y;

  cx.Raw.High &= ~(1 << 16);
  cx.Raw.High |= cy.Raw.High & (1 << 16);
  return cx.Value;
}
