/* pow.c
   Purpose: the implementation of pow. */
#include "fdlibm.h"

double pow(double x, double y)
{
    return __ieee754_pow(x, y);
}
