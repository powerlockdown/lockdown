WORSTC_MATH_FILES=math/abs.c math/__acosX.c math/copysign.c \
	math/e_acosf.c math/e_acosh.c math/e_acoshf.c \
	math/e_acoshl.c math/e_acosl.c math/e_asin.c math/e_asinf.c \
	math/e_asinl.c math/e_atan2.c math/e_atan2f.c math/e_atan2l.c \
	math/e_atanh.c math/e_cosh.c  \
	math/e_coshf.c math/e_coshl.c math/e_exp10.c math/e_exp10f.c  \
	math/e_exp10l.c math/e_exp2.c math/e_exp2f.c math/e_exp2l.c   \
	math/e_exp.c math/e_expf.c math/e_expl.c math/e_fmod.c 	      \
	math/e_fmodf.c math/e_fmodl.c math/e_hypot.c math/e_hypotf.c  \
	math/e_hypotl.c math/e_ilogb.c math/e_ilogbf.c math/e_ilogbl.c \
	math/e_j0.c math/e_j0f.c math/e_j0l.c math/e_j1.c math/e_j1f.c \
	math/e_j1l.c math/e_jn.c math/e_jnf.c math/e_jnl.c	       \
	math/e_lgamma.c math/e_lgammaf.c math/e_lgammaf_r.c	       \
	math/e_lgammal.c math/e_lgammal_r.c math/e_lgamma_r.c          \
	math/e_log10.c math/e_log10f.c math/e_log10l.c math/e_log2.c   \
	math/e_log2f.c math/e_log2l.c math/e_log.c math/e_logf.c       \
	math/e_logl.c math/e_pow.c math/e_powf.c math/e_powi.c         \
	math/e_powif.c math/e_powil.c math/e_powl.c math/e_remainder.c \
	math/e_remainderf.c math/e_remainderl.c math/e_rem_pio2.c      \
	math/e_rem_pio2f.c math/e_rem_pio2l.c math/e_scalb.c           \
	math/e_scalbf.c math/e_scalbl.c math/e_sinh.c math/e_sinhf.c   \
	math/e_sinhl.c math/e_sqrt.c math/e_sqrtf.c math/e_sqrtl.c     \
	math/e_tgammaf_r.c math/e_tgammal_r.c math/e_tgamma_r.c        \
	math/e_y0.c math/e_y0f.c math/e_y0l.c math/e_y1.c math/e_y1f.c \
	math/e_y1l.c math/e_yn.c math/e_ynf.c math/e_ynl.c \
	math/fpclassify.c          \
	math/k_cos.c math/k_cosf.c math/k_cosl.c math/k_rem_pio2.c        \
	math/k_rem_pio2f.c math/k_rem_pio2l.c math/k_sin.c                \
	math/k_sinf.c math/k_sinl.c math/k_standard.c math/k_tan.c        \
	math/k_tanf.c math/k_tanl.c math/__logX.c              \
	math/pow.c math/s_asinh.c math/s_asinhf.c math/s_asinhl.c         \
	math/s_atan.c math/s_atanf.c math/s_atanl.c math/s_cbrt.c         \
	math/s_cbrtf.c math/s_cbrtl.c math/s_ceil.c math/s_ceilf.c        \
	math/s_ceill.c               \
        math/s_cos.c math/s_cosf.c math/s_cosl.c       \
	math/s_erf.c math/s_erfc.c math/s_erfcf.c math/s_erfcl.c          \
	math/s_erff.c math/s_erfl.c math/s_expm1.c math/s_expm1f.c        \
	math/s_expm1l.c math/s_fabs.c math/s_fabsf.c math/s_fabsl.c       \
	math/s_fdim.c math/s_fdimf.c math/s_fdiml.c math/s_finite.c       \
	math/s_finitef.c math/s_finitel.c math/s_floor.c math/s_floorf.c  \
	math/s_floorl.c math/s_fmax.c math/s_fmaxf.c math/s_fmaxl.c       \
	math/s_fmin.c math/s_fminf.c math/s_fminl.c			  \
	math/s_fpclassifyf.c math/s_fpclassifyl.c math/s_frexp.c          \
	math/s_frexpf.c math/s_frexpl.c math/s_isinf.c math/s_isinff.c    \
	math/s_isinfl.c math/s_isnan.c math/s_isnanf.c math/s_isnanl.c    \
	math/s_ldexp.c     \
	math/s_ldexpf.c math/s_ldexpl.c math/s_lib_version.c              \
	math/s_llrint.c math/s_llrintf.c math/s_llrintl.c                 \
	math/s_llround.c math/s_llroundf.c math/s_llroundl.c math/s_log1p.c \
	math/s_log1pf.c math/s_log1pl.c math/s_logb.c math/s_logbf.c math/s_logbl.c \
	math/s_lrint.c math/s_lrintf.c math/s_lrintl.c math/s_lround.c math/s_lroundf.c \
	math/s_lroundl.c math/s_matherr.c math/s_modf.c math/s_modff.c math/s_modfl.c   \
	math/s_nearbyint.c math/s_nearbyintf.c math/s_nearbyintl.c math/s_nextafter.c   \
	math/s_nextafterf.c math/s_nextafterl.c math/s_nexttoward.c math/s_nexttowardf.c \
	math/s_pone.c math/s_ponef.c math/s_ponel.c math/s_pzero.c math/s_pzerof.c math/s_pzerol.c \
	math/s_remquo.c math/s_remquof.c math/s_remquol.c \
	math/s_rint.c math/s_rintf.c math/s_rintl.c math/s_round.c math/s_roundf.c \
	math/s_roundl.c math/s_scalbln.c math/s_scalblnf.c math/s_scalblnl.c \
	math/s_scalbn.c math/s_scalbnf.c math/s_scalbnl.c math/s_signgam.c \
	math/s_significand.c math/s_significandf.c math/s_significandl.c \
	math/s_sin.c math/s_sincos.c math/s_sincosf.c math/s_sincosl.c \
	math/s_sinf.c math/s_sinl.c math/s_tan.c math/s_tanf.c      \
	math/s_tanh.c math/s_tanhf.c math/s_tanhl.c math/s_tanl.c      \
	math/s_trunc.c math/s_truncf.c math/s_truncl.c math/truncxfdf2.c \
	math/t_sincosl.c math/w_gamma.c math/w_gammaf.c math/w_gammaf_r.c \
	math/w_gammal.c math/w_gammal_r.c math/w_gamma_r.c math/w_lgamma.c \
	math/w_lgammaf.c math/w_lgammaf_r.c math/w_lgammal.c math/w_lgammal_r.c \
	math/w_lgamma_r.c math/w_tgamma.c math/w_tgammaf.c math/w_tgammal.c

WORSTC_FENV_FILES=sysdep/fenv/feraiseexcept.c sysdep/fenv/feclearexcept.c sysdep/fenv/fegetenv.c sysdep/fenv/fegetround.c
WORSTC_MATH_FILES+=$(WORSTC_FENV_FILES)
WORSTM_CFLAGS=$(WORSTC_CFLAGS) -Iworstc/math/include

$(WORSTC_BUILD_DIR)/math/%.c.o: $(WORSTC_SOURCE_DIR)/math/%.c
	$(CC) $(WORSTM_CFLAGS) $^ -c -o $@

