/* open.c
   Purpose: the implementaton of open and close. */
#include "errno.h"
#include "fcntl.h"
#include "lockdown/file.h"

#include <power/system.h>

int open(const char* path, int flags, ...)
{
    int transltfla = 0;
    if (flags & (O_RDONLY | O_RDWR)) {
        transltfla |= OPENFILE_READ;
    }

    if (flags & (O_WRONLY | O_RDWR)) {
        transltfla |= OPENFILE_WRITE;
    }

    int sfd = OpenFile(path, transltfla);
    if (sfd < 0) {
        errno = __ecode_into_errno(sfd);
        return -1;
    }

    return sfd;
}

int close(int fd)
{
    CloseFile(fd);
    return 0;
}
