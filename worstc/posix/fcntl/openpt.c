/* openpt.c
   Purpose: The implementation of posix_openpt. */
#include "fcntl.h"
#include "lockdown/file.h"
#include "bits/null.h"
#include "errno.h"

#include <power/pty.h>
#include <stddef.h>

int posix_openpt(int oflag)
{
  (void)oflag;
  int mux = OpenFile("\\??:/ptymux", 0);
  if (mux < 0)
    __return_enoc(mux, -1);
  
  int tty = IoControl(mux, MUX_OPENTTY, NULL, 0);
  if (tty < 0) {
    CloseFile(mux);
    __return_enoc(tty, -1);
  }
  
  CloseFile(mux);
  return tty;
}

int grantpt(int fd)
{
  (void)fd;
  return 0;
}
