/* mmap.c
   Purpose: the implementation of m{un}map. */
#include "lockdown/lockdef.h"
#include "sys/mman.h"
#include "errno.h"

#include <lockdown/memory.h>
#include <power/system.h>

void*
mmap(void* hint, size_t length, int prot, int flags, 
     int fd, __off_t off)
{
    (void)off;
    int realflags = 0;
    __lkd_PTR error = 0;

    if (prot & PROT_WRITE)
        realflags |= VPROT_WRITE;
    if (prot & PROT_EXEC)
        /* Leave sanity checking for the kernel. */
        realflags |= VPROT_EXECUTE;
    if (flags & MAP_ANONYMOUS)
        realflags |= VMAP_ANONYMOUS;
    if (flags & MAP_FIXED)
        realflags |= VMAP_FIXED;

    if (fd != -1) {
        error = (__lkd_PTR)VirtualFileMap(fd, (__lkd_PTR)hint, 
                                          length, realflags);
    } else {
        error = (__lkd_PTR)VirtualMap((__lkd_PTR)hint, length, realflags);
    }

    if (error & 0xFFF) {
        goto fail;
    }

    return (void*)error;
fail:
    errno = __ecode_into_errno(error);
    return MAP_FAILED;
}

int
munmap(void* addr, size_t length)
{
    if (!length) {
        errno = EINVAL;
        return -1;
    }

    VirtualUnmap((__lkd_PTR)addr, length);
    return 0;
}

