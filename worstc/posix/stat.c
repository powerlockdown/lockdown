/* stat.c
   Purpose: the implementation of {f,l}stat.
   TODO: add fstatat support later. */
#include "sys/stat.h"
#include "fcntl.h"
#include "unistd.h"

#include <lockdown/file.h>
#include "power/filestats.h"

int stat(const char* restrict path, struct stat* restrict st)
{
  int fd = open(path, O_RDONLY);
  if (fd == -1)
    return fd; /* errno already set */

  int s;
  s = fstat(fd, st);
  if (s == -1)
    return s; /* errno already set */

  close(fd);
  return 0;
}

int fstat(int fd, struct stat* st)
{
  struct __filestats fs;
  GetFileStatistics(fd, &fs);
  st->st_size = fs.Size;
  st->st_atime = fs.AccessTime;
  st->st_mtime = fs.ModifiedTime;
  st->st_ino = fs.SerialNumber;
  return 0;
}
