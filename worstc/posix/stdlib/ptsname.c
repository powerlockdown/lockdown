/* ptsname.c
   Purpose: The implementation of ptsname */
#include <power/pty.h>
#include <power/limits.h>

#include <lockdown.h>

#include "bits/null.h"
#include "errno.h"

#include <stddef.h>

static char ptsnbuffer[MAX_PATH];

char* ptsname(int tty)
{
  int error;
  if ((error = IoControl(tty, TTY_GETSLN, ptsnbuffer, MAX_PATH)) < 0) {
    __return_enoc(error, NULL);
  }
  
  /* GETSLN does not terminate the str for us. */
  ptsnbuffer[error] = 0;
  return ptsnbuffer;
}
