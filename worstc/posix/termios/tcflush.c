/* tcflush.c
   Purpose: NOP because PTYs never buffer */
#include "termios.h"

int tcflush(int tty, int qs)
{
  return 0;
}
