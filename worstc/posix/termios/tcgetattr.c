/* tcgetattr.c
   Purpose: The implementation of tcgetattr. */
#include "lockdown/file.h"
#include "termios.h"

#include "string.h"
#include <power/pty.h>

#include "bits/null.h"
#include "errno.h"

int tcgetattr(int ftty, struct termios* termi)
{
  int error;
  struct _pty pty;
  memset(&pty, 0, sizeof(pty));
  
  error = IoControl(ftty, TTY_GETCFG, &pty, sizeof(pty));
  if (error < 0) {
    __return_enoc(error, -1);
  }

  memcpy(termi->c_cc, pty.pty_cc, NCCS);
  termi->c_cflag = pty.pty_cflag;
  termi->c_iflag = pty.pty_iflag;
  termi->c_lflag = pty.pty_lflag;
  termi->c_oflag = pty.pty_oflag;
  
  return 0;
}
