/* tcsetattr.c
   Purpose: the implementation of tcsetattr. */
#include "lockdown/file.h"
#include "termios.h"

#include "bits/null.h"
#include "errno.h"

#include <power/pty.h>
#include "string.h"

int tcsetattr(int fd, int ignored, const struct termios* ts)
{
  (void)ignored;

  int error;
  struct _pty pty;
  memcpy(&pty.pty_cc, ts->c_cc, NCCS);
  pty.pty_cflag = ts->c_cflag;
  pty.pty_iflag = ts->c_iflag;
  pty.pty_lflag = ts->c_lflag;
  pty.pty_oflag = ts->c_oflag;

  error = IoControl(fd, TTY_SETCFG, &pty, sizeof(pty));
  if (error < 0)
    __return_enoc(error, -1);

  return 0;
}
