/* _exit.c
   Purpose: The implementation of _exit. */
#include "unistd.h"
#include "signal.h"
#include <lockdown/processthreads.h>

void _exit(int status)
{
  SetExitCode(status);
  ExitProcess();
  
  /* Crash somehow. */
  asm ("hlt");
  __builtin_unreachable();
}

