#include "unistd.h"
#include "fcntl.h"

int access(const char* path, int amode)
{
  int flags = O_RDONLY;
  if (amode & R_OK && amode & W_OK) {
    flags = O_RDWR;
  } else if (amode & W_OK) {
    flags = O_WRONLY;
  }
  
  int fd = open(path, flags);
  if (fd != -1) {
    close(fd);
    return 0;
  }

  /* errno already set. */
  return -1;
}
