/* dup.c
   Purpose: the implementation of dup{2}. */
#include "lockdown/file.h"
#include "lockdown/handle.h"
#include "unistd.h"

#include "bits/null.h"
#include "errno.h"
#include <power/error.h>

int dup(int fd)
{
  int error;
  error = DuplicateHandle(fd);
  if (error < 0)
    __return_enoc(error, -1);

  return error;
}

int dup2(int oldfd, int newfd)
{
  if (GetHandleType(newfd) != -ERROR_BAD_HANDLE) {
    if (oldfd == newfd)
      return newfd;

    CloseFile(newfd);
  }

  /* We just hope f == newfd. */
  int f = dup(oldfd);
  return f;
}


