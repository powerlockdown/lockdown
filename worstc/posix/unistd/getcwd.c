/* getcwd.c
   Purpose: the implementation of getcwd. */
#include "unistd.h"
#include <lockdown/processthreads.h>

#include <stddef.h>
#include "bits/null.h"
#include "errno.h"

char* getcwd(char* buffer, size_t length)
{
  int error = GetCurrentWorkingDir(buffer, length);
  if (error < 0)
    __return_null(error);

  return buffer;
}

int chdir(const char* path)
{
  int error = SetWorkingDirectory(path);
  if (error < 0)
    __return_enoc(error, -1);

  return 0;
}
