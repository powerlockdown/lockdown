/* isatty.c
   Purpose: The implementation of isatty. */
#include "unistd.h"

#include <power/handle.h>
#include <lockdown/processthreads.h>

int isatty(int fd)
{
  return GetHandleType(fd) == HANDLE_TYPE_TTY;
}
