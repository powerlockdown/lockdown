/* sleep.c
   Purpose: the implementation of sleep. */
#include "unistd.h"

#include <lockdown/processthreads.h>
#include "bits/null.h"
#include "errno.h"

int sleep(unsigned secs)
{
  int error;
  error = Sleep(secs);
  if (error < 0)
    __return_enoc(error, -1);

  return 0;
}

