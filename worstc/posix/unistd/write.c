/* write.c
   Purpose: the implementation of write. */
#include "unistd.h"

#include <lockdown/file.h>

int write(int fd, const void* buffer, int length)
{
    return WriteFile(fd, buffer, length);
}

int read(int fd, void* buffer, int length)
{
    return ReadFile(fd, buffer, length);
}

