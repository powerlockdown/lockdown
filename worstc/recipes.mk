WORSTC_BUILD_DIR=build/wc
WORSTC_SOURCE_DIR=worstc
WORSTC_INCLUDE_DIR=worstc/include

WORSTC_OUTPUT=$(WORSTC_BUILD_DIR)/libworstc.so
WORSTM_OUTPUT=$(WORSTC_BUILD_DIR)/libworstm.so

include worstc/mathsrc.mk

# ============================ FILES
WORSTC_LOCALE_FILES=locale/setlocale.c
WORSTC_MALLOC_FILES=malloc/liballoc_interface.c malloc/liballoc.c
#WORSTC_MATH_FILES=math/acos.c math/fpclassify.c math/sqrt.c math/abs.c math/log.c \
#				  math/pow.c math/fdm_pow.c math/fdm_scalbn.c math/copysign.c math/e_log2.c math/e_log2f.c math/elog2l.c
WORSTC_STDLIB_FILES=stdlib/strtoul.c stdlib/abs.c stdlib/abort.c stdlib/div.c \
					stdlib/atexit.c stdlib/strtoXYfloat.c stdlib/getenv.c stdlib/atoi.c
WORSTC_POSIX_FILES=posix/unistd/write.c posix/fcntl/open.c posix/mman/mmap.c posix/stat.c posix/unistd/getcwd.c posix/unistd/_exit.c posix/fcntl/openpt.c posix/stdlib/ptsname.c posix/termios/tcgetattr.c posix/termios/tcsetattr.c posix/unistd/sleep.c posix/unistd/access.c posix/termios/tcflush.c posix/unistd/dup.c posix/unistd/isatty.c
WORSTC_STDIO_FILES=stdio/fopen.c stdio/getc.c stdio/fread.c stdio/stdout.c \
				   stdio/feof.c stdio/fseek.c stdio/printf.c stdio/buffering.c stdio/fput.c \
				   stdio/fflush.c stdio/printf_digs.c stdio/setvbuf.c stdio/fwrite.c \
				   stdio/sscanf.c
WORSTC_STRING_FILES=string/strcpy.c string/memset.c string/strlen.c string/memcmp.c \
					string/memchr.c string/strspn.c string/memmove.c string/strtok.c \
					string/strbrk.c string/strcat.c string/strstr.c string/strerror.c \
					string/strcoll.c
WORSTC_STRINGS_FILES=strings/ffs.c
WORSTC_SYSDEP_FILES=sysdep/longjmp.S sysdep/ifarray.c sysdep/pthread_self.c sysdep/pthread_restore.S
WORSTC_THREAD_FILES=thread/pthread_key.c thread/mt_create.c thread/tls_key.c thread/pthread_specific.c \
		    thread/tq.c thread/pthread_mutex.c thread/pthread_create.c thread/pthread_event.c
WORSTC_TIME_FILES=time/time.c time/mktime.c time/gmtime.c time/difftime.c
WORSTC_SIGNAL_FILES=signal/signal.c signal/sigemptyset.c signal/sigwait.c signal/sigprocmask.c signal/kill.c
WORSTC_FILES=start.c $(WORSTC_LOCALE_FILES) \
					 $(WORSTC_STRING_FILES) $(WORSTC_MALLOC_FILES) $(WORSTC_STDIO_FILES) \
					 		$(WORSTC_STDLIB_FILES) $(WORSTC_STRINGS_FILES) \
							$(WORSTC_SYSDEP_FILES) $(WORSTC_POSIX_FILES) $(WORSTC_TIME_FILES) $(WORSTC_THREAD_FILES) \
							$(WORSTC_SIGNAL_FILES) errno.c assert.c $(WORSTC_MATH_FILES)
# =============================

WORSTM_SOURCES=$(addprefix $(WORSTC_SOURCE_DIR)/,$(WORSTC_MATH_FILES))
WORSTM_BUILD=$(addprefix $(WORSTC_BUILD_DIR)/,$(addsuffix .o,$(WORSTC_MATH_FILES)))

WORSTC_SOURCES=$(addprefix $(WORSTC_SOURCE_DIR)/,$(WORSTC_FILES))
WORSTC_BUILD=$(addprefix $(WORSTC_BUILD_DIR)/,$(addsuffix .o,$(WORSTC_FILES)))
WORSTC_HEADERS=$(shell find $(WORSTC_INCLUDE_DIR)/ -type f -name '*.h')

# Ugly hack for install to place the headers inside their correct directories 
WORSTC_INSTALLED_HEADERS=$(addprefix $(SYSTEM_ROOT)/System/Headers/,$(subst $(WORSTC_INCLUDE_DIR)/,,$(WORSTC_HEADERS)))
include worstc/wcrt.mk

WORSTC_CFLAGS=$(CFLAGS) -iquote $(WORSTC_INCLUDE_DIR) -Wno-expansion-to-defined -D__MATH_STATIC
WORSTC_LDFLAGS=-shared -Wl,-z -Wl,now -nostdlib -fPIC
WORSTC_LDFLAGS+=-L$(WORSTC_BUILD_DIR) -lworstm -L$(LKD_BUILD_DIR) -llockdown
WORSTC_ASFLAGS=-g -I$(WORSTC_INCLUDE_DIR) -fPIC

include worstc/static.mk

worstc: $(WORSTC_OUTPUT)

worstc_installheaders: $(WORSTC_INSTALLED_HEADERS)

worstc_install: worstc worstc_installheaders
	install -C -t $(SYSTEM_ROOT)/System/Libraries $(WORSTC_OUTPUT) -v

worstm_install: $(WORSTM_OUTPUT)
	install -C -t $(SYSTEM_ROOT)/System/Libraries $(WORSTM_OUTPUT) -v

$(SYSTEM_ROOT)/System/Headers/%.h: $(WORSTC_INCLUDE_DIR)/%.h
	install -C -D -t $(SYSTEM_ROOT)/System/Headers/$(dir $(subst $(WORSTC_INCLUDE_DIR)/,,$^)) $^ -v

$(WORSTC_OUTPUT): $(WORSTC_BUILD) worstm_static
	$(LD) $(WORSTC_LDFLAGS) $(WORSTC_BUILD) $(WORSTC_POST_LDFLAGS) -lgcc_s -o $(WORSTC_OUTPUT)

$(WORSTM_OUTPUT): $(WORSTM_BUILD)
	$(LD) $(WORSTC_LDFLAGS) $(WORSTM_BUILD) $(WORSTC_POST_LDFLAGS) -o $(WORSTM_OUTPUT)

$(WORSTC_BUILD_DIR)/%.c.o: $(WORSTC_SOURCE_DIR)/%.c
	mkdir -p build/$(dir $(subst $(WORSTC_SOURCE_DIR)/,wc/,$^))
	$(CC) $(WORSTC_CFLAGS) $^ -c -o $@

$(WORSTC_BUILD_DIR)/%.S.o: $(WORSTC_SOURCE_DIR)/%.s
	mkdir -p build/$(dir $(subst $(WORSTC_SOURCE_DIR)/,wc/,$^))
	$(CC) $(WORSTC_ASFLAGS) $^ -c -o $@

$(WORSTC_SOURCE_DIR)/%.s: $(WORSTC_SOURCE_DIR)/%.S
	$(PREPROCESS.S) $(WORSTC_ASFLAGS) $< > $@

worstc_clean:
	rm -rf $(WORSTC_BUILD_DIR)
	mkdir -p $(WORSTC_BUILD_DIR)
