/* kill.c
   Purpose: The implementation of kill. */
#include "signal.h"
#include <lockdown/signal.h>
#include "stdlib.h"

#include "bits/null.h"
#include "errno.h"

int kill(pid_t pid, int sig)
{
  if (!pid || pid == -1)
    return raise(sig);

  int error = SendProcessSignal(abs(pid), sig);
  if (error < 0)
    __return_enoc(error, -1);
  return 0;
}
