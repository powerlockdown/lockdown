/* sigemptyset.c
   Purpose: the implementation of sigemptyset. */
#include "signal.h"

int sigaddset(sigset_t* set, int sig)
{
  *set |= 1 << sig;
  return 0;
}

int sigdelset(sigset_t* set, int sig)
{
  *set &= ~(1 << sig);
  return 0;
}

int sigemptyset(sigset_t* set)
{
  (*set) = 0;
  return 0;
}

int sigfillset(sigset_t* set)
{
  (*set) = ~0UL;
  return 0;
}

