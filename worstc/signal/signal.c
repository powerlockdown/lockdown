/* signal.c
    Purpose: the implementation of signal. */
#include "signal.h"

#include <power/system.h>
#include <lockdown.h>

static __signal_handler handlers[64];

static void __call_sig(int code);

__signal_handler 
signal(int sig, __signal_handler han)
{
    handlers[sig] = han;
    SigHandler(sig, __call_sig);
    return han;
}

static void 
__call_sig(int code)
{
    if (handlers[code]) {
        (handlers[code])(code);
    }

    SystemCall(SYS_RETFSIGNAL, 0, 0, 0, 0);
}

int raise(int signal)
{
    return RaiseSignal(signal);
}
