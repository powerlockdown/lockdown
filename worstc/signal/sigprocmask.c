/* sigprocmask.c
   Purpose: the implementation of sigprocmask. */
#include "signal.h"
#include "bits/null.h"
#include "errno.h"

#include <lockdown/signal.h>

int sigprocmask(int how, const sigset_t* restrict set,
                sigset_t* restrict oset)
{
  int type;
  switch (how) {
  case SIG_BLOCK:
    type = SIGMASK_MASK;
    break;
  case SIG_UNBLOCK:
    type = SIGMASK_UNMASK;
    break;
  case SIG_SETMASK:
    type = SIGMASK_SET;
    break;
  default:
    __return_value(EINVAL, -1);
  }

  unsigned long nmask;
  nmask = SetSignalMask(type, *set);
  
  if (oset)
    *oset = nmask;

  return 0;
}
