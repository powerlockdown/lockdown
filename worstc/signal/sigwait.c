/* sigwait.c
   Purpose: the implementation of sigwait. */
#include "signal.h"
#include <lockdown/signal.h>

int sigwait(sigset_t* restrict set, int* restrict sig)
{
  int si = WaitForSignal(*set);
  (*sig) = si;
  return 0;
}
