#include "start.h"
#include "bits/atexit.h"

#include <lockdown.h>
#include <stddef.h>

typedef void (*__init_function)(void);

static __init_function* __finistart = NULL;
static __init_function* __finiend = NULL;

static void(*__fini)() = NULL;

void __libc_call_main(int(*main)(int argc, char** argv), 
                      void(*init)(), 
                      void(*fini)(),

                      __init_function* preinitstart,
                      __init_function* preinitend,
                      __init_function* initstart,
                      __init_function* initend,
                      __init_function* finistart,
                      __init_function* finiend)
{
    __fini = fini;
    __finistart = finistart;
    __finiend = finiend;

    init();

    __libc_ifini_init(preinitstart, preinitend, initstart, initend);
    __libc_exit(main(0, NULL));
}

/* Calls _fini. */
void __libc_exit(int code)
{
    __atex_dispatch_handlers();
    
    if (__fini)
        __fini();
    
    __libc_ifini_fini(__finistart, __finiend);

    SetExitCode(code);
    ExitProcess();

    __builtin_unreachable();
}
