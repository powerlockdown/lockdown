/* start.h
   Purpose: Generic startup code */
#pragma once

#include "bits/config.h"

typedef void (*__init_function)(void);

void 
__libc_ifini_init(__init_function* preinitstart,
                  __init_function* preinitend,
                  __init_function* initstart,
                  __init_function* initend);

void __libc_ifini_fini(__init_function* finistart,
                       __init_function* finiend);

__noret
void __libc_exit(int code);
