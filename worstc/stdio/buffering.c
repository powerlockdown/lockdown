/* buffering.c
   Purpose: Implementation of buffering */
#include <lockdown.h>
#include "buffering.h"
#include "bits/FILE.h"
#include "string.h"

#ifdef __GNUC__
#define __assume(C) do { if(!(C)) __builtin_unreachable(); } while(0)
#else
#define __assume(C)
#endif

#define min(X, y) (X < y) ? (y) : (X)

/* Write *length* bytes of *buffer* and increment *file*'s internal pointers,
   whilst respecting internal bfr buffer boundaries.
   
   Returns non-zero (if writing the whole buffer would go out of bounds)
           or zero (if the entire buffer could be written) */
static int __bfr_advance(FILE* file, const char* buffer, size_t length)
{
    int oob = 0;
    if (length > file->f_bfr_length - file->f_bfr_position) {
        length = file->f_bfr_length - file->f_bfr_position;
        oob = 1;
    }

    memcpy(file->f_bfr_buffer + file->f_bfr_position, buffer, length);
    file->f_bfr_position += length;
    return oob ? length : 0;
}

/* Submit the bfr buffer to the system and empty it out.  */
int __bfr_flush(FILE* file)
{
    if (!file->f_bfr_position)
        return 0;

    int r;
    r = WriteFile(file->f_handle, file->f_bfr_buffer, file->f_bfr_position);
    memset(file->f_bfr_buffer, 0, file->f_bfr_length);
    file->f_bfr_position = 0;

    return r;
}

/* Write *buffer* to the bfr, and optionally flushes it if
   a newline is found (for line-buffered streams) or if
   *buffer* does not fit entirely onto bfr. */
int __bfr_write_buffer(FILE* file, const char *buffer, size_t length)
{
    switch (file->f_bfr_mode) {
    case __FBUFFERING_LINE: {
        size_t idx = strcspn(buffer, "\n");
        int r = length + idx + 1;
        int e;

        int of = __bfr_advance(file, buffer, idx);

        /* The remainder of the string (after the \n) 
                did not fit the buffer. */
        if (idx != length || of) {
            /* Empty the buffer... */
            e = __bfr_flush(file);
            if (e < 0) {
                r = e;
            }

            /* ...and then push the rest. */
            __bfr_advance(file, buffer + idx + 1, length - (idx + 1));
        }

        return r;
    }
    case __FBUFFERING_BUFFER: {
        size_t idx;
        int r = 0, e;

        if ((idx = __bfr_advance(file, buffer, length))) {
            e = __bfr_flush(file);
            r = length + idx;
            if (e < 0) {
                r = e;
            }

            __bfr_advance(file, buffer + idx, length - (idx));
        }
        return r;
    }
    default:
        break;
    }

    __assume(file->f_bfr_mode == __FBUFFERING_NONE);
    int r = WriteFile(file->f_handle, buffer, length);
    return r;
}
