#pragma once

#include "stdio.h"

int __bfr_flush(FILE* file);
int __bfr_write_buffer(FILE* file, const char* buffer, size_t length);
