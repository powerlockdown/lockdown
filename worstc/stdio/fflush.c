/* fflush.c
   Purpose: the implementation of fflush. */
#include "errno.h"
#include "stdio.h"

#include "buffering.h"

int fflush(FILE* stream)
{
    int error;
    if ((error = __bfr_flush(stream)) < 0) {
        errno = __ecode_into_errno(error);
        return -1;
    }

    return error;
}
