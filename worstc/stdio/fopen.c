/* fopen.c
   Purpose: The implementation of f{{re}open,close} */
#include "bits/FILE.h"
#include "lockdown/file.h"
#include "stdio.h"

#include <lockdown.h>
#include "stdlib.h"
#include "string.h"

#define UNGETC_SIZE 30

static int getopenflags(const char* restrict mode)
{
    int flags = 0;
    if (mode[0] == 'r') {
        flags |= OPENFILE_READ;
        if (mode[1] == '+' 
         || (mode[1] == 'b' && mode[2] == '+')) {
            flags |= OPENFILE_WRITE;
        }
    }

    return flags;
}

FILE* fopen(const char* restrict name, 
            const char* restrict mode)
{
    int flags = getopenflags(mode);
    if (!flags) {
        return NULL;
    }

    int handle = OpenFile(name, flags);
    if (handle < 0) {
        return NULL;
    }

    FILE* f = malloc(sizeof(FILE));
    memset(f, 0, sizeof(FILE));
    
    f->f_handle = handle;
    f->f_buffer_stack = calloc(UNGETC_SIZE, sizeof(char));
    return f;
}

int fclose(FILE* stream)
{
    int error;
    error = fflush(stream);

    CloseFile(stream->f_handle);
    if (!(stream->f_flags & __FILE_STATIC)) {
        free(stream->f_buffer_stack);
        free(stream);
    }

    if (error > 0) {
        return 0;
    }

    /* errno was set by fflush. */
    return EOF;
}

FILE*
freopen(const char* restrict filename,
        const char* restrict mode,
        FILE* restrict stream)
{
    fflush(stream);

    int flags = getopenflags(mode);
    if (!flags) {
        return NULL;
    }

    CloseFile(stream->f_handle);
    stream->f_handle = OpenFile(filename, flags);
    if (stream->f_handle < 0) {
        return NULL;
    }

    return stream;
}

int fileno(FILE* stream)
{
    return stream->f_handle;
}

FILE* fdopen(int handle, const char* mode)
{
    (void)mode;
    FILE* f = malloc(sizeof(FILE));
    memset(f, 0, sizeof(FILE));
    
    f->f_handle = handle;
    f->f_buffer_stack = calloc(UNGETC_SIZE, sizeof(char));
    return f;
}

