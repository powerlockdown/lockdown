/* fput.c
   Purpose: The implementation of fput{c,s} and putchar */
#include "bits/FILE.h"
#include "buffering.h"
#include "stdio.h"
#include "string.h"

int fputc(int c, FILE* stream)
{
    char string[2] = { c, 0 };
    __bfr_write_buffer(stream, string, 1);
    return 1;
}

int putchar(int c)
{
    fputc(c, stdout);
    if (feof(stdout)) {
        return EOF;
    }

    return c;
}

int fputs(const char* restrict s, FILE* stream)
{
    size_t len = strlen(s);
    __bfr_write_buffer(stream, s, len);
    return len;
}
