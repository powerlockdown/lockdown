/* fread.c
   Purpose: Implementation of fread. */
#include "stdio.h"
#include "bits/FILE.h"

#include <lockdown.h>
#include <power/error.h>

size_t
fread(void* restrict buffer, size_t size, 
      size_t memberSize, FILE* f)
{
   size_t rs = size;
   char* restrict cb = buffer;
   while(f->f_bs_index) {
      *cb++ = __pop_buffer_char(f);
      if (f->f_bs_pos % memberSize == 0)
         rs--;
   }

   int rd = ReadFile(f->f_handle, cb, rs * memberSize);
   
   if (rd < 0)
      f->f_flags |= __FILE_ERROR;
   else if (!rd || rd == -ERROR_PIPE_BROKEN)
      f->f_flags |= __FILE_EOF;

   f->f_position += rd;
   return rd / memberSize;
}
