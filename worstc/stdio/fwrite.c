/* fwrite.c
   Purpose: the implementation of fwrite. */
#include "bits/FILE.h"
#include "buffering.h"
#include "stdio.h"

size_t fwrite(const void* restrict buffer, size_t size, 
              size_t count, FILE* restrict stream)
{
    long l = size * count;
    long error;

    error = __bfr_write_buffer(stream, buffer, l);
    if (error < 0) {
        stream->f_flags |= __FILE_ERROR;
    } else if (error != l) {
        stream->f_flags |= __FILE_EOF;
    }

    return error;
}
