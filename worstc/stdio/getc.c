/* getc.c
   Purpose: The implementation of {un,f}getc */
#include "power/system.h"
#include "power/error.h"
#include "stdio.h"
#include <lockdown.h>

int __pop_buffer_char(FILE* stream)
{
    unsigned char c = stream->f_buffer_stack[stream->f_bs_pos];
    stream->f_bs_pos++;
    if (stream->f_bs_pos == stream->f_bs_index) {
        stream->f_bs_pos = 0;
        stream->f_bs_index = 0;
    }

    return c;
}

int ungetc(int c, FILE* stream)
{
    unsigned char cw = c;
    stream->f_buffer_stack[stream->f_bs_index] = cw;
    stream->f_bs_index++;
    return cw;
}

int fgetc(FILE* stream)
{
    if (stream->f_bs_index) {
        return __pop_buffer_char(stream);
    }

    char c;
    int error = ReadFile(stream->f_handle, &c, 1);
    if (error == -ERROR_PIPE_BROKEN) {
        return EOF;
    }

    SetFilePointer(stream->f_handle, SFILEPTR_REL_CURRENT, 1);
    stream->f_position++;
    return c;
}
