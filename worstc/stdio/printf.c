/* printf.c
   Purpose: The implementation of {f}printf */
#include "stdio.h"
#include "printf.h"

#include "errno.h"
#include <stdarg.h>
#include "stdlib.h"
#include "string.h"

static void __parse_single_spec(const char* c, va_list args, 
                                struct __printf_spec* spec);
static int __parse_spec(const char* ptr, va_list args, 
                        struct __printf_spec* spec);

static ptrdiff_t __parse_int(signed int i, int base, char* out, 
                             struct __printf_spec* sp);
static unsigned long __writex(struct __printf_spec* sp,
                              const char* string, size_t length);

/* {v}fprintf: 
    Functions that write onto a stream,
    while optionally accepting a va_args instead
    of a ellipsis. */
int fprintf(FILE* stream, const char* msg, ...)
{
    int ret;
    va_list list;
    va_start(list, msg);

    ret = vfprintf(stream, msg, list);
    va_end(list);

    return ret;
}

int vfprintf(FILE* stream, const char* msg, va_list args)
{
    struct __printf_spec spec;
    memset(&spec, 0, sizeof(spec));

    spec.Stale = 0;
    spec.Stream = stream;
    while (*msg) {
        if (*msg != '%') {
            fputc(*msg, stream);
            goto cont;
        }

        msg += __parse_spec(msg, args, &spec);
        spec.Stale = 1;
cont:
        msg++;
   }

   return 0;
}

/* {v}s{n}printf: 
    Functions that dump their results 
    onto a user-supplied buffer, while 
    optionally accepting a va_list 

    instead of a ellipsis, and optionally 
    specifying a maximum length. */
int
vsnprintf(char* __WC_restrict buffer, size_t bsize, 
          const char* msg, va_list va)
{
    struct __printf_spec spec;
    memset(&spec, 0, sizeof(spec));

    size_t index = 0;
    spec.Stale = 0;
    spec.Flags = _PRINTF_BUFFER_BIT;
    spec.MaxLength = bsize;
    spec.Buffer = buffer;

    while (msg[index] && index < bsize) {
        if (msg[index] != '%') {
            *(buffer + index) = msg[index];
            goto cont;
        }

        index += __parse_spec(msg + index, va, &spec);
        spec.Stale = 1;
cont:
        index++;
   }

   return index;
}

int 
vsprintf(char* __WC_restrict buffer, 
         const char* __WC_restrict msg, 
         va_list va)
{
    struct __printf_spec spec;
    memset(&spec, 0, sizeof(spec));

    const char* begin = msg;

    spec.Stale = 0;
    spec.Flags = _PRINTF_BUFFER_BIT;
    spec.MaxLength = -1ULL;
    spec.Buffer = buffer;

    while (*msg) {
        if (*msg != '%') {
            *(buffer++) = *msg;
            goto cont;
        }

        msg += __parse_spec(msg, va, &spec);
        spec.Stale = 1;
cont:
        msg++;
   }

   return msg - begin;
}

int 
sprintf(char* restrict buffer, const char* restrict msg, ...)
{
    va_list va;
    va_start(va, msg);

    int ret = vsprintf(buffer, msg, va);

    va_end(va);
    return ret;
}

extern int 
snprintf(char* restrict buffer, size_t bsize, const char* restrict msg, ...)
{
    va_list va;
    va_start(va, msg);

    int ret = vsnprintf(buffer, bsize, msg, va);

    va_end(va);
    return ret;
}


/* {v,}printf: Functions that behave as if 
            fprintf was called for stdout. */
int 
vprintf(const char* __WC_restrict msg, va_list va)
{
    return vfprintf(stdout, msg, va);
}

int 
printf(const char* __WC_restrict msg, ...)
{
    int ret;
    va_list list;
    va_start(list, msg);

    ret = vfprintf(stdout, msg, list);
    va_end(list);
    return ret;
}

int __parse_spec(const char* ptr, va_list args, 
                 struct __printf_spec* spec)
{
    spec->Stale = _PRINTF_STATE_PRECISION;

    const char* begin = ptr;
    do {
        ptr++; /* The % spec */
        __parse_single_spec(ptr, args, spec);
    } while (*(ptr + 1) != ' ' && *(ptr + 1) != '\n' &&
	   *(ptr + 1) == '\0');

    return ptr - begin;
}

void __parse_single_spec(const char* ptr, va_list args, 
                         struct __printf_spec* spec)
{
    switch (*ptr) {
    case '*': {
        int value = va_arg(args, int);
        if (spec->Stale == _PRINTF_STATE_WIDTH) {
            spec->FieldWidth = value;
            spec->Stale = _PRINTF_STATE_PRECISION;
        }
        else if (spec->Stale == _PRINTF_STATE_PRECISION) {
            spec->Precision = value;
            spec->Stale = _PRINTF_STATE_IGNORE;
        }

        /* _STATE_IGNORE will match right here. */
        break;
    }

    /* ---- LENGTH MODIFIERS ---- */
    case 'h': {
        if (*(ptr - 1) == 'h')
            spec->LengthMod = _PRINTF_LENGTH_MOD_CHAR;
        else
            spec->LengthMod = _PRINTF_LENGTH_MOD_SHORT;
        break;
    }
    case 'l': {
        if (*(ptr - 1) == 'l')
            spec->LengthMod = _PRINTF_LENGTH_MOD_LONGLONG;
        else
            spec->LengthMod = _PRINTF_LENGTH_MOD_LONG;
        break;
    }
    /* j, z, t and L length modifiers. */

    /* ---- CONVERSION SPECIFIERS ---- */
    /* "The number of characters that can be 
            produced by any single conversion 
            shall be at least 4095."
        but ain't no way I be doin' that. */
    case 'i': {
        char t[64];
        int i = va_arg(args, int);

        ptrdiff_t length = __parse_int(i, 10, t, spec);
        __writex(spec, t, length);
        break;
    }
    case 's': {
	char* ptr = va_arg(args, void*);
	size_t length;
	
	if (!ptr)
	    ptr = "(null)";

	length = strlen(ptr);
	__writex(spec, ptr, length);	
	break;
    }
    
    
    }
}

ptrdiff_t __parse_int(signed int i, int base, char* out, 
                 struct __printf_spec* sp)
{
    static char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";

    const char* ptr = out;
    if (!sp->Precision && !i) {
        /* The result of converting a zero value 
           with a precision of zero is no characters */
        return 0;
    }

    if (i < 0) {
        *out++ = '-';
        i *= -1;
    }

    if (base == 10 && sp->Precision) {
        int reqdg = __get_required_digits_b10(abs(i));
        int paddg = sp->Precision - reqdg;
        if (paddg > 0) {
            for (int i = 0; i < paddg; i++) {
                (*out++) = '0';
            }
        }
    }

    
    int shifter = i;

    /* A buffer overflow might be yielded 
       from here. */
    do {
        out++;
        shifter /= base;
    } while (shifter);

    (*out) = '\0';
    do {
        *(--out) = digits[i % base];
        i /= base;
    } while (i);

    return out - ptr;
}

static 
unsigned long __writex(struct __printf_spec* sp,
             const char* string, size_t length)
{
    if (sp->Flags & _PRINTF_BUFFER_BIT) {
        if (sp->CurrentIndex >= sp->MaxLength) {
            return -ENOSPC;
        }

        if (sp->CurrentIndex + length > sp->MaxLength) {
            length = sp->MaxLength - sp->CurrentIndex;
        }

        memcpy(sp->Buffer + sp->CurrentIndex, string, length);
        sp->CurrentIndex += length;
        return length;
    }

    return fwrite(string, sizeof(char), 
                  length, sp->Stream);
}
