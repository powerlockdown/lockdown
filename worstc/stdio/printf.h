/* printf.h 
   Purpose: definitions used by the printf state machine */
#pragma once

#include "bits/FILE.h"

/* This printf should write to a memory buffer (set) or 
    to a file stream (clear). */
#define _PRINTF_BUFFER_BIT (1 << 0) 

enum {
    _PRINTF_LENGTH_MOD_CHAR = 1,
    _PRINTF_LENGTH_MOD_SHORT,
    _PRINTF_LENGTH_MOD_INT,
    _PRINTF_LENGTH_MOD_LONG,
    _PRINTF_LENGTH_MOD_WIDE = _PRINTF_LENGTH_MOD_LONG, /*< wchar_t or wint_t */
    _PRINTF_LENGTH_MOD_LONGLONG
};

enum {
    _PRINTF_STATE_IGNORE,
    _PRINTF_STATE_PRECISION,
    _PRINTF_STATE_WIDTH,
    _PRINTF_STATE_MORE
};

struct __printf_spec {
    int Stale;
    int Flags;
    int LengthMod;
    int FieldWidth;
    int Precision;

    size_t MaxLength; /*< For BUFFER_BIT. */
    size_t CurrentIndex; /*< For BUFFER_BIT. */
    
    union { /* Check for BUFFER_BIT before
                using these fields! */
        struct _FILE_struct* Stream;
        char* Buffer;
    };
};

int __get_required_digits_b10(long int number);
