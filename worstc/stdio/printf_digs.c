/* printf_digs.c
   Purpose: compute how many digits are required
            to represent certain number. Used in
            printf. */
#include "printf.h"
#include "bits/config.h"

int __get_required_digits_b10(long int number)
{
#if __has_builtin(__builtin_clz)
#define K(T) (((sizeof(#T) - 1ull) << 32) - T)
  static const unsigned long long table[] = {
      K(0),          K(0),          K(0),
      K(10),         K(10),         K(10),
      K(100),        K(100),        K(100),
      K(1000),       K(1000),       K(1000),
      K(10000),      K(10000),      K(10000),
      K(100000),     K(100000),     K(100000),
      K(1000000),    K(1000000),    K(1000000),
      K(10000000),   K(10000000),   K(10000000),
      K(100000000),  K(100000000),  K(100000000),
      K(1000000000), K(1000000000), K(1000000000),
      K(1000000000), K(1000000000)
  };
  return (number + table[__builtin_clz(number | 1) ^ 31]) >> 32u;
#else
    int r = 1;
    if (n < 0) n = (n == INT_MIN) ? INT_MAX: -n;
    while (n > 9) {
        n /= 10;
        r++;
    }
    return r;
#endif
}
