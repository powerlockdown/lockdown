/* setvbuf.c
   Purpose: The implementation of set{v}buf. */
#include "bits/FILE.h"
#include "stdio.h"
#include "stdlib.h"

int
setvbuf(FILE* restrict stream, char* restrict buffer,
        int mode, size_t size)
{
    if ((stream->f_bfr_mode & __FBUFFERING_OWNERSHIP)) {
        free(stream->f_bfr_buffer);
        stream->f_bfr_mode &= ~__FBUFFERING_OWNERSHIP;
    }

    if (buffer) {
        stream->f_bfr_buffer = buffer;
        stream->f_bfr_length = size;
    } else {
        buffer = calloc(size, 1);
        if (!buffer) {
            /* TODO set errno. */
            return -1;
        }

        stream->f_bfr_mode |= __FBUFFERING_OWNERSHIP;
    }

    stream->f_bfr_mode &= ~7;
    switch (mode) {
    case _IOFBF:
        stream->f_bfr_mode |= __FBUFFERING_BUFFER;
        break;
    case _IOLBF:
        stream->f_bfr_mode |= __FBUFFERING_LINE;
        break;
    default: /* case _IONBF: */
        break;
    }

    return 0;
}

void setbuf(FILE* restrict stream, char* restrict buffer)
{
    if (buffer) {
        setvbuf(stream, buffer, _IOFBF, BUFSIZ);
        return;
    }

    setvbuf(stream, NULL, _IONBF, 0);
}
