/* sscanf.c
   Purpose: the implementation of {f,s}scanf. */
#include "stdio.h"
#include "printf.h"

#include "string.h"
#include "errno.h"
#include <stdarg.h>

#include "ctype.h"
#include "stdlib.h"

#define _SCANF_BUFFER_SIZE 96

struct __scanf_spec {
    struct __printf_spec sf_spec;

    int sf_index;
    int sf_size;
    char* sf_buffer;
};

static int readx(struct __scanf_spec* spec, 
                   char* restrict buffer, 
                   int length);

static 
unsigned int __parse_number(struct __scanf_spec* spec, int first,
                            unsigned long* out)
{
    char c;
    readx(spec, &c, 1);

    char digits[3];
    digits[0] = first;
    digits[1] = c;
    digits[2] = 0;

    (*out) = strtoul(digits, NULL, 10);
    return c;
}

static ptrdiff_t __handle_spec(struct __scanf_spec* spec,
                          const char* format, va_list va)
{
    char c = *format;
    if (c == '%' && spec->sf_spec.Stale == _PRINTF_STATE_IGNORE) {
        spec->sf_spec.Stale = _PRINTF_STATE_MORE;
        return 1;
    }

    char* end;
    unsigned long numb;
    if (isdigit(c)) {
        numb = strtoul(format, &end, 10);
        spec->sf_spec.FieldWidth = numb;
        return end - format;
    }

    int base, i;
    switch (c) {
    case 'd':
        base = 10;
        goto nb;
    case 'i': {
        base = 0;
nb:
        i = 0;
        char dx[30];
        do {
            readx(spec, dx + i, 1);
        } while (i < 30 && isdigit(dx[i++]));

        int* num = va_arg(va, int*);
        (*num) = strtol(dx, NULL, base);
        break;
    }
    }

    return 1;
}

int 
sscanf(const char* restrict buffer, const char* restrict format, ...)
{
    va_list va;
    va_start(va, format);
    
    int ret = vsscanf(buffer, format, va);
    va_end(va);

    return ret;
}

int vsscanf(const char* restrict bf, const char* restrict format, va_list va)
{
    char buffer[_SCANF_BUFFER_SIZE];

    struct __scanf_spec spc;
    memset(&spc, 0, sizeof(spc));

    spc.sf_spec.Buffer = (char*)bf;
    spc.sf_buffer = buffer;
    spc.sf_size = _SCANF_BUFFER_SIZE;
    spc.sf_spec.Stale = _PRINTF_STATE_IGNORE;
    spc.sf_spec.Flags = _PRINTF_BUFFER_BIT;

    while (*format) {
        if (*format == '%' 
         || spc.sf_spec.Stale != _PRINTF_STATE_IGNORE) {
            format += __handle_spec(&spc, format, va) - 1;
        }

        format++;
    }
}

int
fscanf(FILE* restrict stream, const char* restrict format, ...)
{
    va_list va;
    va_start(va, format);
    int result = vfscanf(stream, format, va);

    va_end(va);
    return result;
}

int 
vfscanf(FILE* restrict stream, const char* restrict format, va_list va)
{
    char rcs;
    char buffer[_SCANF_BUFFER_SIZE];

    struct __scanf_spec spc;
    memset(&spc, 0, sizeof(spc));

    spc.sf_spec.Stream = stream;
    spc.sf_buffer = buffer;
    spc.sf_size = _SCANF_BUFFER_SIZE;
    spc.sf_spec.Stale = _PRINTF_STATE_IGNORE;

    while (*format) {
        if (*format == '%' 
         || spc.sf_spec.Stale != _PRINTF_STATE_IGNORE) {
            format += __handle_spec(&spc, format, va) - 1;
        }

        format++;
    }
}

static inline
int __iseof(struct __scanf_spec* spec)
{
    if (!(spec->sf_spec.Flags & _PRINTF_BUFFER_BIT)) {
        if (feof(spec->sf_spec.Stream)) {
            return EOF;
        }
    }

    return spec->sf_index == spec->sf_size;
}

static
int __readmore(struct __scanf_spec* spec)
{
    if (spec->sf_spec.Flags & _PRINTF_BUFFER_BIT) {
        return -EOPNOTSUPP;
    }

    size_t rl;
    rl = fread(spec->sf_buffer, spec->sf_size, 
            sizeof(char), spec->sf_spec.Stream);
    spec->sf_size = rl;
    spec->sf_index = 0;
    return rl;    
}

static 
int readx(struct __scanf_spec* spec, char* restrict buffer, int length)
{
    int oldlength = 0;
    int readmore = 0;
    if (length >= spec->sf_size - spec->sf_index) {
        oldlength = length;
        if (__iseof(spec) == EOF) {
            /* Nothing we can do. 
               Just clamp it. */
            length = spec->sf_size - spec->sf_index;
        } else
            readmore = 1;

        if (!length) {
            return 0;
        }
    }

    memcpy(buffer, spec->sf_spec.Buffer + spec->sf_index, 
            length);
    spec->sf_index += length;
    if (readmore) {
        __readmore(spec);
        oldlength -= length;
        memcpy(buffer + length, spec->sf_spec.Buffer, 
            oldlength);
    }

    return length;
}
