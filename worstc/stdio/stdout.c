/* stdout.c
   Purpose: the definition of std{err,out,in} */
#include "stdio.h"
#include "bits/FILE.h"
#include "power/system.h"

#include <stddef.h>

static char stdoutBuffer[128];
static char stderrBuffer[256];

static FILE _stdout_inner = {
    NULL, 0, 0, 0, 0, sizeof(stdoutBuffer), stdoutBuffer, 
    __FBUFFERING_LINE, STREAM_STDOUT, __FILE_WRITE_ONLY | __FILE_STATIC
};

static FILE _stdin_inner = {
    NULL, 0, 0, 0, 0, 0, NULL, 
    __FBUFFERING_NONE, STREAM_STDIN, __FILE_STATIC
};

static FILE _stderr_inner = {
    NULL, 0, 0, 0, 0, 0, NULL, 
    __FBUFFERING_NONE, STREAM_STDERR, __FILE_STATIC
};

extern FILE __attribute__ (( alias("_stdout_inner") )) __stdout;
extern FILE __attribute__ (( alias("_stdin_inner") )) __stdin;
extern FILE __attribute__ (( alias("_stderr_inner") )) __stderr;
