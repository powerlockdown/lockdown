/* XYdiv.c
   Purpose: generic implementation of {l{l}}div. */

/* * DIV_TYPE - return div type,
   * INT_TYPE - integer arg type,
   * DIV_FUNC - resulting function name */

DIV_TYPE DIV_FUNC(INT_TYPE numer, INT_TYPE denom)
{
    DIV_TYPE div;
    div.quot = numer / denom;
    div.rem = numer % denom;

    if (numer >= 0 && div.rem < 0) {
        div.quot++;
        div.rem -= denom;
    }

    return div;
}

#undef DIV_TYPE
#undef INT_TYPE
#undef DIV_FUNC
