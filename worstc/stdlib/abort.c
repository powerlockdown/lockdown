/* abort.c
   Purpose: the implementation of abort and exit */
#include "stdlib.h"

#include "lockdown.h"
#include "../start.h"

__noret
void abort()
{
    /* TODO: add a print message. */
    ExitProcess();
    __builtin_unreachable();
}

__noret
void exit(int code)
{
    __libc_exit(code);
}
