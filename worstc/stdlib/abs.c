/* abs.c
   Purpose: Implementation of abs */
#include "stdlib.h"

int abs(int val)
{
    return val < 0 ? val * -1 : val;
}

long labs(long val)
{
    return val < 0 ? val * -1 : val;
}

long long llabs(long long val)
{
    return val < 0 ? val * -1 : val;
}
