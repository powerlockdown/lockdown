/* atexit.c
   Purpose: the implementation of atexit. */
#include "bits/atexit.h"
#include <stddef.h>

#define ATEX_MAX 50

typedef void(*__atexit2)(void*);

struct __atex_handler {
    __atexit2 atx_proc;
    void* atx_arg;
};

static int __atex_count = 0;
static struct __atex_handler __atex_handlers[ATEX_MAX];

int atexit(__atexit _proc)
{
    if (__atex_count >= ATEX_MAX) {
        return -1;
    }

    __atex_handlers[__atex_count].atx_proc = _proc;
    __atex_handlers[__atex_count].atx_arg = NULL;
    __atex_count++;
    return 0;
}

void __atex_dispatch_handlers()
{
    for (int i = 0; i < __atex_count; i++) {
        (*(__atex_handlers[i].atx_proc))(__atex_handlers[i].atx_arg);
    }
}

/* The C++ compiler generates calls to this
   whenever it wants to register destructors. */
int __cxa_atexit(void (*func)(void *), void* arg, void* dso)
{
    (void)dso;
    if (__atex_count >= ATEX_MAX) {
        return -1;
    }

    __atex_handlers[__atex_count].atx_proc = func;
    __atex_handlers[__atex_count].atx_arg = arg;
    __atex_count++;
    return 0;
}
