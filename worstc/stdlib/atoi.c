/* atoi.c
   Purpose: The implementation of ato{i,l{l},f} */
#include "stdlib.h"

double atof(char* str)
{
    return strtod(str, NULL);
}

int atoi(char* str)
{
    return (int)strtol(str, NULL, 10);
}

long atol(char* str)
{
    return strtol(str, NULL, 10);
}

long long
atoll(char* str)
{
    return strtoll(str, NULL, 10);
}

