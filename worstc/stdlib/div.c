/* div.c
   Purpose: the implementation of {l{l}}div. */
#include "bits/div.h"

/* These functions only exist because pre-C99 division and modulo
    had unspecified rounding direction for negative numbers. */

#define DIV_FUNC div
#define DIV_TYPE div_t
#define INT_TYPE int

#include "XYdiv.c"

#define DIV_FUNC ldiv
#define DIV_TYPE ldiv_t
#define INT_TYPE long

#include "XYdiv.c"

#define DIV_FUNC lldiv
#define DIV_TYPE lldiv_t
#define INT_TYPE long long

#include "XYdiv.c"

