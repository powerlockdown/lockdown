/* strtoXYfloat.c
   Purpose: generic implementation for strto{{l}d,f} */
#include "stdlib.h"
#include "math.h"

#ifndef STRTOXYFLOAT_NAME
#define STRTOXYFLOAT_NAME strtod
#endif

#ifndef STRTOXYFLOAT_TYPE
#define STRTOXYFLOAT_TYPE double
#endif

STRTOXYFLOAT_TYPE
STRTOXYFLOAT_NAME(const char* restrict string, char** restrict end)
{
    STRTOXYFLOAT_TYPE output = 0;
    int neg = *string == '-';

    if (*string == '-' || *string == '+') {
        string++;
    }

    int base = 10;
    int index = 0;
    int scien;

    char* endbuffer;
    const char* ptr = string;

    while (*ptr) {
        index = (*ptr) - '0';
        if (*ptr == 'e') {
            scien = strtol(++ptr, &endbuffer, 10);

            ptr = endbuffer - 1;
            output *= pow(10, scien);
            goto c;
        }
        
        if (*ptr == '.') {
            scien = strtoul(++ptr, &endbuffer, 0);
            output += scien / pow(10, (endbuffer - ptr));

            ptr = endbuffer - 1;
            goto c;
        }

        /* I don't know if this is standard. */
        if (*ptr == 'f') {
            goto ret;
        }

        if (index > base) {
            /* TODO set errno */
            return 0.0f;
        }

        output *= base;
        output += index;
c:
        ptr++;
    }

ret:
    if (neg)
        output *= -1;
    if (end)
        (*end) = (char*)ptr;
    return output;
}

#undef STRTOXYFLOAT_NAME
#undef STRTOXYFLOAT_TYPE
