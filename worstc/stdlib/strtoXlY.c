/* strtoXlY.c
   Purpose: Generic implementation for strto{u}{l}l. */
#include <stdlib.h>

/* Return type of generated function */
#ifndef STRTOXLY_TYPE
#define STRTOXLY_TYPE unsigned long int
#endif

/* Name of generated function */
#ifndef STRTOXLY_FUNCTION
#define STRTOXLY_FUNCTION strtoul
#endif

/* Add code paths for handling negative numbers (defined) or not (not defined). */
#ifndef STRTOXLY_SIGNED
/* #define STRTOXLY_SIGNED 1 */
#endif

STRTOXLY_TYPE 
STRTOXLY_FUNCTION(const char* _string, char** _endptr, int _bs)
{
    while (*_string == ' ') {
        _string++;
    }

#ifdef STRTOXLY_SIGNED
    int neg = 0;
#endif

    if (*_string == '+' || *_string == '-') {
        _string++;
#ifdef STRTOXLY_SIGNED
        neg = 1;
#endif
    }
        

    if (!_bs) {
        _bs = 10;
        if (*_string == '0') {
            _bs = 8;
            _string++;
            if (*(_string) == 'x'
             || *(_string) == 'X') {
                _bs = 16;
                _string++;
            }
        }
    }

    int index;
    STRTOXLY_TYPE output = 0;
    while (*_string) {
        index = (*_string) - '0';        
        if (*_string >= 'a')
            index = (*_string) - 87;
        else if (*_string >= 'A')
            index = (*_string) - 55;

        if (index > _bs) {
            /* TODO set errno */
            return 0;
        }

        output *= _bs;
        output += index;        
        _string++;
    }

    if (_endptr)
	    (*_endptr) = (char*)_string;

#ifdef STRTOXLY_SIGNED
    if (neg)
        output *= -1;
#endif
    return output;
}

#ifdef STRTOXLY_SIGNED
#   undef STRTOXLY_SIGNED
#endif

#undef STRTOXLY_FUNCTION
#undef STRTOXLY_TYPE
