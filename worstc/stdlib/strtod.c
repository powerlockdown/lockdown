/* strtod.c
   Purpose: the implementation of strtod. */

/* generates strtod. */
#include "strtoXYfloat.c"

/* generates strtof. */
#define STRTOXYFLOAT_NAME strtof
#define STRTOXYFLOAT_TYPE float
#include "strtoXYfloat.c"

/* generates strtold. */
#define STRTOXYFLOAT_NAME strtold
#define STRTOXYFLOAT_TYPE long double
#include "strtoXYfloat.c"