/* strtoul.c
   Purpose: implementation of strtoul */

/* generates strtoul. */
#include "strtoXlY.c"

/* generates strtoull. */
#define STRTOXLY_FUNCTION strtoull
#define STRTOXLY_TYPE unsigned long long int
#include "strtoXlY.c"

/* generates strtol. */
#define STRTOXLY_SIGNED
#define STRTOXLY_FUNCTION strtol
#define STRTOXLY_TYPE signed long int
#include "strtoXlY.c"

/* generates strtoll. */
#define STRTOXLY_SIGNED
#define STRTOXLY_FUNCTION strtoll
#define STRTOXLY_TYPE signed long long int
#include "strtoXlY.c"

