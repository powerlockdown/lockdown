/* strcat.c
   Purpose: the implementation of str{n}cat. */
#include "string.h"

char* strcat(char* restrict str1, 
             const char* restrict str2)
{
    return strncat(str1, str2, strlen(str2));
}

char* strncat(char* restrict str1, 
              const char* restrict str2, size_t ctc)
{
    char* restrict pt = str1;
    while (*pt) {
        pt++;
    }

    memmove(pt, str2, ctc);
    *(pt + ctc + 1) = 0;
    return str1;
}

