/* strcoll.c
   Purpose: The implementation of str{coll,xfrm} */
#include "string.h"

int 
strcoll(const char* str1, const char* str2)
{
    return strcmp(str1, str2);
}

extern size_t 
strxfrm(char* __WC_restrict _str1, const char* __WC_restrict _str2, size_t _ctc)
{
    return 0;
}

