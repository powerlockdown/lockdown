/* strtok.c
   Purpose: the implementation of strtok{_r}. */
#define _POSIX_SOURCE
#include "string.h"

static char* _strtokBuffer;

char* strtok(char* __WC_restrict _string, const char* __WC_restrict _sep)
{
    return strtok_r(_string, _sep, &_strtokBuffer);
}

char* 
strtok_r(char* __WC_restrict _string, const char* __WC_restrict _sep, char** _ctx)
{
    if (!*_ctx)
        (*_ctx) = _string;

    if (!*_ctx && !_string)
	return NULL;

    if (!_string)
	_string = *_ctx;

    char* ptr;
    while (*_sep) {
        if ((ptr = strchr(*_ctx, *_sep))) {
            (*ptr) = 0;
            (*_ctx) = ptr + 1;
            return _string;
        }

        _sep++;
    }

    ptr = *_ctx;
    (*_ctx) = NULL;
    return ptr;
}

