#include "fenv.h"

int feclearexcept(int exc)
{
  uint32_t mxcsr;
  asm ("stmxcsr %0" : "=m"(*&mxcsr));
  
  mxcsr &= ~(exc & FE_ALL_EXCEPT);
  asm ("ldmxcsr %0" :: "m"(*&mxcsr));
  return 0;
}
