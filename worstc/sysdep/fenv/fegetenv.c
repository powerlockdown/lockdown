/* fegetenv.c
   Purpose: the implementation of fe{{get,set}env,holdexcept}*/
#include "fenv.h"

int fegetenv(fenv_t* env)
{
  asm ("ldmxcsr %0" : "=m"(*env));
  return 0;
}

int fesetenv(fenv_t* env)
{
  (*env) &= 0xFFFF;
  asm ("stmxcsr %0" :: "m"(env));

  return 0;
}

int feholdexcept(fenv_t* env)
{
  fenv_t e = *env;
  fegetenv(env);

  /* Mask all exceptions. */
  e |= 0x1f80;

  fesetenv(&e);
  return 0;
}
