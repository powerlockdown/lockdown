/* fegetround.c
   Purpose: The implementation of fe{get,set}round. */
#include "fenv.h"

int fegetround()
{
  fenv_t f;
  fegetenv(&f);

  return (f & (3 << 13)) >> 13;
}

int fesetround(int round)
{
  fenv_t f;
  fegetenv(&f);
  f &= ~(3 << 13);
  f |= round << 13;
  
  fesetenv(&f);
  return 0;
}
