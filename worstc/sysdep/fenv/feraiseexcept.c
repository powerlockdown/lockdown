/* fenv.c
   Purpose: purposefully fire an
            FP exception and all of its
            consequences. */
#include "fenv.h"

int feraiseexcept(int exc)
{  
  if (exc & FE_DIVBYZERO) {
    float x = 1.0f;
    float y = 0.0f;
    
    asm volatile("divss %1, %0 " :: "x"(x), "x"(y));
  }
  
  return 0;
}
