/* ifarray.c
   Purpose: support for initfini arrays. */
#include <sys/types.h>
#include "../start.h"

void 
__libc_ifini_init(__init_function* preinitstart,
                  __init_function* preinitend,
                  __init_function* initstart,
                  __init_function* initend)
{
    size_t count, i;
    count = preinitend - preinitstart;

    for (i = 0; i < count; i++) {
        preinitstart[i]();
    }

    count = initend - initstart;
    for (i = 0; i < count; i++) {
        initstart[i]();
    }
}

void __libc_ifini_fini(__init_function* finistart,
                       __init_function* finiend)
{
    size_t count, i;
    count = finiend - finistart;
    for (i = 0; i < count; i++) {
        finistart[i]();
    }
}

