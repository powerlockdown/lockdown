/* pthread_restore.S
   Purpos: fix registers before calling pthread_exit */

.global __pthread_restore
.align 1 << 4
__pthread_restore:
	mov %rax, %rdi
	call pthread_exit@plt
	ret
