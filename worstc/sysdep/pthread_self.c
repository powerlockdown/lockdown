/* pthread_self.c
   Purpose: the implementation of pthread_self. */
#include "bits/pthread.h"

struct __pthread* __pthread_self()
{
  struct __pthread* self;
  asm ("mov %%fs:0, %0" : "=rm"(self));
  return self;
}
