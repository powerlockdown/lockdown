/* mt_create.c
   Purpose: create the main pthread */
#include "pthread.h"
#include "bits/pthread.h"

#include <lockdown.h>
#include <power/fsgs.h>

#include "stdlib.h"
#include "string.h"
#include <stdint.h>

extern int main_called;

struct __pthread*__create_thread_object()
{
  struct __pthread* pth = malloc(sizeof(*pth));
  if (!pth)
    /* Report an error or something else. */
    return NULL;
  
  memset(pth, 0, sizeof(*pth));
  pth->pth_self = pth;
  return pth;
}

void __destroy_thread_object(struct __pthread* object)
{
  free(object);
}

void __set_thread_object(struct __pthread* object)
{
  SystemCall(SYS_FSGSCTL, FSGS_WRITE | FSGS_FS, (uintptr_t)object,0,0);
  main_called = 1;
}


