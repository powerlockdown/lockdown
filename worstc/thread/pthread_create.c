/* pthread_create.c
   Purpose: create a new thread */
#include "bits/mutex.h"
#include "lockdown.h"
#include "bits/atomic.h"

#include "power/system.h"
#include "power/error.h"
#include "power/limits.h"

#include "pthread.h"
#include "bits/pthread.h"

#include "errno.h"
#include "stdlib.h"
#include "bits/null.h"

extern void
__pthread_restore();    

static void*
__pthread_start(void* thread);

static void
__pthread_dispatchod(struct __pthread* thread);

int pthread_create(pthread_t* restrict thread,
                   const pthread_attr_t* restrict attrib,
		   void *(*main)(void *), void* ctx)
{
  if (!thread)
    __return_value(EINVAL, -1);

  int error;
  struct __pthread* pt;
  
  pt = __create_thread_object();
  if (!pt)
    __return_value(ENOMEM, -1);

  pt->pth_context = ctx;
  pt->pth_entry = main;
  (*thread) = pt;

  /* Directly employ SystemCall because Lockdown
     does not expose the restorer argument. */
  error = SystemCall(SYS_CREATETHREAD, (long)__pthread_start,
	             (long)*thread, 0, (long)__pthread_restore);
  
  if (error == -ERROR_TOO_MANY_HANDLES) {
    __destroy_thread_object(pt);
    __return_value(EAGAIN, -1);
  }

  pt->pth_id = error;
  return 0;
}

void* __pthread_start(void* data)
{
  struct __pthread* thread;
  thread = data;
  
  __set_thread_object(thread);
  return thread->pth_entry(thread->pth_context);
}

void pthread_exit(void* value)
{
  (void)value;

  int otid;
  struct __pthread* self;
  self = __pthread_self();

  otid = self->pth_id;
  __pthread_dispatchod(self);
  
  if (self->pth_keyarray)
    free(self->pth_keyarray);

  free(self);
  KillThread(otid);
}

/* Tell events currently owned by
   this thread that it died.*/
void __pthread_dispatchod(struct __pthread* thread)
{
  struct __pthread_event* et;
  et = (struct __pthread_event*)thread->pth_evlist.lkl_next;
  for (; et; et = (struct __pthread_event*)et->__ev_list.lkl_next) {
    __pthread_event_odnotify(et);
  }
}
