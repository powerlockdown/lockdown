/* pthread_event.c
   Purpose: pthread_event. */
#include "bits/mutex.h"

#include <lockdown/processthreads.h>

void __pthread_event_odnotify(struct __pthread_event* event)
{
  if (event->__ev_flags & _PTHEV_OWNER_DIED_BIT)
    /* Still has an unhandled OD notification. */
    return;

  event->__ev_flags |= _PTHEV_OWNER_DIED_BIT;
  if (event->__ev_waiter)
      UnsuspendThread(event->__ev_waiter);
}
