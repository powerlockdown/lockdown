/* pthread_key.c
   Purpose: thread specific keys */
#include "pthread.h"
#include "tls_key.h"

#include "limits.h"
#include "errno.h"
#include "stdlib.h"

#include "bits/null.h"
#include "bits/pthread.h"

int pthread_key_create(pthread_key_t* pk, void (*dtor)(void*))
{
  int key;
  struct __pthread_key* ko;

  if (!pk)
    __return_value(EINVAL, -1);
  
  key = __tls_alloc();
  
  if (key == INT_MIN)
    __return_value(EAGAIN, -1);

  ko = calloc(1, sizeof(*ko));
  if (!ko)
    __return_value(ENOMEM, -1);

  ko->ptk_dtor = dtor;
  ko->ptk_key = key;
  
  (*pk) = ko;
  return 0;
}

int pthread_key_delete(pthread_key_t pk)
{
  struct __pthread_key* ko;
  ko = pk;

  if (!ko)
    __return_value(EINVAL, -1);

  __tls_free(ko->ptk_key);
  free(ko);
  return 0;
}
