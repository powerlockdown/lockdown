/* pthread_mutex.c
   Purpose: the implementation of a mutex. */
#include "bits/lklist.h"
#include "bits/mutex.h"
#include "bits/null.h"
#include "lockdown/processthreads.h"

#include "pthread.h"
#include "bits/pthread.h"

#include "errno.h"

int pthread_mutex_lock(pthread_mutex_t* mutex)
{
  int thr;
  int ret = 0;
  
  struct __pthread* self;
  self = __pthread_self();

  /*The mutex is already locked. */ 
  if (mutex->__mtx_owner) {

    /* But there is no one waiting.
       We shall be the first among them. */
    if (!mutex->__mtx_base.__ev_waiter) {
      mutex->__mtx_base.__ev_waiter = self->pth_id;
      /* Someone is already waiting. Jump into the queue. */
    } else {
      __tq_push(&mutex->__mtx_waiters, self->pth_id);
    }

    SuspendThread(self->pth_id);
    /* The owner died whilst holding the mutex. */
    if (mutex->__mtx_base.__ev_flags & _PTHEV_OWNER_DIED_BIT) {
      ret = -1;

      mutex->__mtx_base.__ev_flags &= ~_PTHEV_OWNER_DIED_BIT;
      thr = __tq_pop(&mutex->__mtx_waiters);
      if (thr == -1 && errno == ENODATA)
	thr = 0;
	
      errno = EOWNERDEAD;
      mutex->__mtx_base.__ev_waiter = thr;
    }
  }

  mutex->__mtx_owner = self->pth_id;
  __klist_insert(&self->pth_evlist, &mutex->__mtx_base.__ev_list);
  return ret;
}

int pthread_mutex_unlock(pthread_mutex_t* mutex)
{
  int olderrno = errno;
  struct __pthread* self;
  self = __pthread_self();

  /* ONLY if mutex is ERRORCHECK or RECURSIVE. */
  if (mutex->__mtx_owner != self->pth_id)
    __return_value(EPERM, -1);

  int oldw;
  if (mutex->__mtx_base.__ev_waiter) {
    oldw = mutex->__mtx_base.__ev_waiter;
    mutex->__mtx_base.__ev_waiter = __tq_pop(&mutex->__mtx_waiters);
    
    if (mutex->__mtx_base.__ev_waiter == -1
	&& errno == ENODATA)
      mutex->__mtx_base.__ev_waiter = 0;
    
    UnsuspendThread(oldw);
  }

  if (!__klist_remove(&mutex->__mtx_base.__ev_list))
    self->pth_evlist.lkl_next = NULL;
 
  errno = olderrno;
  return 0;
}

