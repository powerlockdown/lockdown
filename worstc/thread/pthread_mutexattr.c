/* pthread_mutexattr.c
   Purpose: The implementation of mutexattr functions. */
#include "bits/config.h"
#include "bits/null.h"
#include "bits/pthread_attr.h"
#include "pthread.h"

int pthread_mutexattr_init(pthread_mutexattr_t* attr)
{
  attr->mf_robustness = PTHREAD_MUTEX_STALLED;
  attr->mf_type = PTHREAD_MUTEX_DEFAULT;
  return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int type)
{
  if (__WC_unlikely(type > __MUTEXATTR_TYPE_MAX))
    __return_value(EINVAL, -1);

  attr->mf_type = type;
  return 0;
}

int pthread_mutexattr_setrobust(pthread_mutexattr_t* attr, int rob)
{
  if (__WC_unlikely(rob > __MUTEXATTR_ROBUSTNESS_MAX))
    __return_value(EINVAL, -1);

  attr->mf_robustness = rob;
  return 0;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t* a)
{
  (void)a;
  return -1;
}
