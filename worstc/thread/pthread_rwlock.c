/* pthread_rwlock.c
   Purpose: The implementation of readers-writers lock. */
#include "bits/mutex.h"
#include "bits/pthread.h"
#include "lockdown/processthreads.h"
#include "pthread.h"
#include "bits/null.h"

#include "assert.h"

#define BIL_RET_ERR 1

/* LOck the RWL for reading. */
static
int __rwlock_rdlock(pthread_rwlock_t* rwl, int bil)
{
  struct __pthread* self;
  self = __pthread_self();

  if (!rwl->__rw_owner) {
  becown:
    rwl->__rw_uses++;
    rwl->__rw_owner = self->pth_id;
    rwl->__rw_flags &= ~_RWLOCK_OWNER_WRITER;
    return 0;
  }

  /* Currently busy. But are they writer?
     If so not busy for us. */
  if (!(rwl->__rw_flags & _RWLOCK_OWNER_WRITER)) {
    /* TODO: RWLs can have multiple owners when reading.
       
       What happens here is that the first reader is still
       the owner and when it unlocks, all readers should also
       stop reading because a writer might come and destroy
       everything. */
    rwl->__rw_uses++;
    return 0;
  }

  if (bil == BIL_RET_ERR)
    __return_value(EBUSY, -1);
  
  rwl->__rw_nreaders++;
  __tq_push(&rwl->__rw_readers, self->pth_id);
  SuspendThread(self->pth_id);
  
  goto becown;
  __builtin_unreachable();
}

/* Lock the RWL for writing. */
static
int __rwlock_wrlock(pthread_rwlock_t* rwl, int bil)
{
  struct __pthread* self;
  self = __pthread_self();
  
  /* No owner? Take it for ourselves. */
  if (!rwl->__rw_owner) {
  becown:
    rwl->__rw_owner = self->pth_id;
    rwl->__rw_flags |= _RWLOCK_OWNER_WRITER;
    goto ret;
  }
  
  if (rwl->__rw_owner == self->pth_id)
    __return_value(EDEADLK, -1);

  /* (We are currently going to be locked.)
     Is our callee rwlock_trywrlock?
     Tell them we are busy.  */
  if (bil == BIL_RET_ERR)
    __return_value(EBUSY, -1);

  rwl->__rw_nwriters++;
  __tq_push(&rwl->__rw_writers, self->pth_id);
  SuspendThread(self->pth_id);
  goto becown;
  
 ret:
  return 0;
}

int pthread_rwlock_rdlock(pthread_rwlock_t* rwl)
{
  return __rwlock_rdlock(rwl, 0);
}

int pthread_rwlock_tryrdlock(pthread_rwlock_t* rwl)
{
  return __rwlock_rdlock(rwl, BIL_RET_ERR);
}

int pthread_rwlock_wrlock(pthread_rwlock_t* rwl)
{
  return __rwlock_wrlock(rwl, 0);
}

int pthread_rwlock_trywrlock(pthread_rwlock_t* rwl)
{
  return __rwlock_wrlock(rwl, BIL_RET_ERR);
}

static
int __rwlock_unlock(pthread_rwlock_t* rwl)
{
  int td;
  
  if (rwl->__rw_flags & _RWLOCK_PREFER_WRITERS) {
    if (!rwl->__rw_nwriters)
      goto nowr;

  nord:
    if (!rwl->__rw_nwriters) {
      rwl->__rw_owner = 0;
      return 0;
    }

    td = __tq_pop(&rwl->__rw_writers);
    
    assert(td != -1);
    UnsuspendThread(td);
    
    /* The other thread is responsible
       for setting itself as owner
       (see __rwlock_wrlock). */    
  } else {
    /* Prefers readers. */
  nowr:
    if (!rwl->__rw_nreaders)
      goto nord;

    rwl->__rw_uses = rwl->__rw_nreaders;
    while (rwl->__rw_nreaders) {
      td = __tq_pop(&rwl->__rw_readers);
      
      assert(td != 1);
      UnsuspendThread(td);
      rwl->__rw_nreaders--;
    }
  }
    
  return 0;
}

int pthread_rwlock_unlock(pthread_rwlock_t* rwl)
{
  /* This is necessary for multiple readers. */
  rwl->__rw_uses--;
  if (rwl->__rw_uses)
    return 0;
    
  return __rwlock_unlock(rwl);
}

