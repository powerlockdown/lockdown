#include "pthread.h"
#include "bits/pthread.h"

#include "errno.h"
#include "stdlib.h"

#include "bits/null.h"

int pthread_setspecific(pthread_key_t key, const void* value)
{
  struct __pthread_key* ko;
  ko = key;
  
  struct __pthread* self;
  self = __pthread_self();

  if (!ko)
    __return_value(EINVAL, -1);
  
  if (__builtin_expect(!!(!self->pth_keyarray), 0))
    self->pth_keyarray = calloc(_PTHR_KEYS, sizeof(uintptr_t));

  if (!self->pth_keyarray)
    __return_value(ENOMEM, -1);

  self->pth_keyarray[ko->ptk_key] = (uintptr_t)value;
  return 0;
}

void* pthread_getspecific(pthread_key_t key)
{
  struct __pthread_key* ko;
  ko = key;

  struct __pthread* self;
  self = __pthread_self();
  
  if (!key)
    __return_value(EINVAL, (void*)(uintptr_t)-1);

  if (!self->pth_keyarray)
    return NULL;

  return (void*)self->pth_keyarray[ko->ptk_key];
}


