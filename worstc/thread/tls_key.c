/* tls_key.c
   Purpose: the inner implementation of thread specific keys. */
#include "pthread.h"
#include "bits/pthread.h"
#include "limits.h"

#include <stddef.h>
#include <stdlib.h>
#include "assert.h"

struct _tls_alloc {
  int ta_cindex;
  int ta_length;
  
  unsigned int* ta_array;
};

static struct _tls_alloc* _tls = NULL;

static void __tls_create();

int __tls_alloc()
{
  if (!_tls)
    __tls_create();

  if (_tls->ta_cindex >= _tls->ta_length)
    return INT_MIN;

  return _tls->ta_array[++_tls->ta_cindex];
}

void __tls_free(int index)
{
  assert(_tls->ta_cindex);
  _tls->ta_array[--_tls->ta_cindex] = index;
}

void __tls_create()
{
  _tls = calloc(1, sizeof(*_tls));
  _tls->ta_length = _PTHR_KEYS;
  _tls->ta_array = calloc(_PTHR_KEYS, sizeof(unsigned int));

  /* Everything is free. */
  for (int i = 0; i < _tls->ta_length; i++) {
    _tls->ta_array[i] = i;
  }
}
