/* tls_key.h
   Purpose: TLS index manager. */
#pragma once

int __tls_alloc();
void __tls_free(int index);

