/* tq.c
   Purpose: the thread queue. */
#include "bits/mutex.h"

#include "bits/null.h"
#include "errno.h"
#include "stdlib.h"

#define PTHR_TQ_LENGTH 4

static
size_t grow_initial_buffer(struct __pthread_thrqueue* tq)
{
  tq->__tq_buffer = calloc(PTHR_TQ_LENGTH, sizeof(int));
  if (!tq->__tq_buffer)
    return -1ULL;
  
  tq->__tq_capacity = PTHR_TQ_LENGTH;
  tq->__tq_length = 0;

  return PTHR_TQ_LENGTH;
}

int __tq_pop(struct __pthread_thrqueue* tq)
{
  if (!tq->__tq_length) {
    __return_value(ENODATA, -1);
  }

  int old = tq->__tq_buffer[tq->__tq_length - 1];
  tq->__tq_length--;
  return old;
}
  
int __tq_push(struct __pthread_thrqueue* tq, int val)
{
  if (!tq->__tq_capacity
      || tq->__tq_length >= tq->__tq_capacity)
    if (grow_initial_buffer(tq) == -1ULL)
      __return_value(ENOMEM, -1);
  
  tq->__tq_buffer[tq->__tq_length++] = val;
  return tq->__tq_length;
}

