/* difftime.c
   Purpose: Provide the difference
            between two calendar times. */
#include "time.h"

double difftime(time_t t1, time_t t0)
{
  return t1 - t0;
}
