/* gmtime.c
   Purpose: convert time_t to struct tm. */
#include "bits/time.h"
#include "time.h"

#include "assert.h"

static struct tm gmtimer;

/* Note that we do not have
   any notion of timezones and
   we presume (correctly in some cases)
   that all timers are in UTC already. */

struct tm* gmtime_r(const time_t* restrict ti,
		    struct tm* restrict result)
{
  time_t mon;
  time_t day;
  time_t diff;
  time_t epc = *ti;
  time_t yeal = epc;
  
  if ((diff = yeal % __SECONDS_PER_YEAR)) {
    yeal -= diff;
  }

  epc -= yeal;
  diff = 0;
  result->tm_year = (yeal / __SECONDS_PER_YEAR) + 1970;

  for (int i = 0; i < 12; i++) {
    mon = __cal_getdaycount(i, result->tm_year);
    diff += mon * __SECONDS_PER_DAY;
    
    if (diff >= epc) {
      result->tm_mon = i + 1;
      diff -= mon * __SECONDS_PER_DAY;
      epc -= diff;
    }
  }

  assert(result->tm_mon);
  diff = 0;
  day = epc;
  
  /* Calculate the day, hour and minute. */
  if ((diff = epc % 86400)) {
    day -= diff;
  }
  
  result->tm_mday = day / 86400;

  /* I'm real lazy to declare
     more variables so just reuse them*/
  epc -= day;

  /* Calculate the HOUR. */
  if ((diff = epc % 3600)) {
    day = epc - diff;
  }

  result->tm_hour = day / 3600;

  epc -= day;

  /* Calculate the MINUTE. */
  if ((diff = epc % 60)) {
    day = epc - diff;
  }

  result->tm_min = day / 60;
  epc -= day;

  /* Calculating the SECOND is really easy. */
  result->tm_sec = epc;
  return result;
}

struct tm* gmtime(const time_t* t)
{
  return gmtime_r(t, &gmtimer);
}

/* See the start of this file for
   the reason why this is an alias.*/
struct tm* localtime(const time_t* t)
{
  return gmtime(t);
}

struct tm* localtime_r(const time_t* t, struct tm* restrict timer)
{
  return gmtime_r(t, timer);
}

