/* mktime.c
   Purpose: The implementation of mktime. */
#include "time.h"
#include "bits/time.h"

#define SECONDS_PER_YEAR 31556926
#define SECONDS_PER_DAY  86400

static struct tm tobj;

static 
time_t wcintotimer(const struct tm* waclk)
{
    time_t result = 0;
    int absyear = waclk->tm_year + 1900;
    int year = absyear - 1970;
    result += (year - 1) * SECONDS_PER_YEAR;
    for (int i = 1; i < (waclk->tm_mon - 1); i++) {
        result += __cal_getdaycount(i, absyear) * SECONDS_PER_DAY;
    }

    result += (waclk->tm_mday - 1) * SECONDS_PER_DAY;
    result += (waclk->tm_hour - 1) * 3600;
    result += (waclk->tm_min - 1) * 60;
    result += waclk->tm_sec;
    return result;
}

time_t mktime(struct tm* tm)
{
  return wcintotimer(tm);
}
