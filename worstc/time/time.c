/* time.c
   Purpose: the implmentation of time. */
#include "time.h"
#include "errno.h"
#include <lockdown/time.h>

#define SECONDS_PER_YEAR 31556926
#define SECONDS_PER_DAY  86400

/* TAKEN FROM THE KERNEL!! */

static inline
int calisleap(int ye)
{
    return (ye % 400) == 0 || 
           (ye % 4 == 0 && ye % 100 != 0);
}

int __cal_getdaycount(int mo, int ye)
{
    /* Odd months until July have 31 days. */
    if (mo < 8 && mo % 2 != 0) {
        return 31;
    }

    /* Beginning from August, even months
       are the ones to have 31 days. */
    if (mo >= 8 && !(mo % 2)) {
        return 31;
    }
    
    /* February is special, though. */
    if (mo == 2) {
        if (calisleap(ye))
            return 29;
        return 28;
    }

    /* The rest has thirty. */
    return 30;
}

static 
time_t wcintotimer(const WALLCLOCK* waclk)
{
    time_t result = 0;
    int year = waclk->wk_year - 1970;
    result += (year - 1) * __SECONDS_PER_YEAR;
    for (int i = 1; i < (waclk->wk_month - 1); i++) {
        result += __cal_getdaycount(i, waclk->wk_year) * __SECONDS_PER_DAY;
    }

    result += waclk->wk_day * SECONDS_PER_DAY;
    result += waclk->wk_hour * 3600;
    result += waclk->wk_minute * 60;
    result += waclk->wk_second;
    return result;
}

time_t time(time_t* timer)
{
    int status;

    time_t result;
    WALLCLOCK waclk = {0};
    if ((status = GetTimeOfDay(&waclk)) < 0) {
        errno = __ecode_into_errno(status);
        return -1;
    }
    
    result = wcintotimer(&waclk);

    if (timer)
        (*timer) = result;
    return result;
}

size_t strftime(char *restrict s, size_t maxsize,
           const char *restrict format, const struct tm *restrict timeptr)
{
    return 0;
}
