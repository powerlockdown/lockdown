WCRT_OUTPUT=$(addprefix $(WORSTC_BUILD_DIR)/,wcruntime.o crti.o crtbegin.o crtend.o crtn.o)

WCRT_FILES=wcrt/crt0.s
WCRT_SOURCES=$(addprefix $(WORSTC_SOURCE_DIR)/,$(WCRT_FILES))
WCRT_BUILD=$(addprefix $(WORSTC_BUILD_DIR)/,$(addsuffix .o,$(WCRT_FILES)))

wcrt_install: wcrt
# Because WCRT source files count will likely never grow, we can use this
# hardcode them.
	cp $(WORSTC_BUILD_DIR)/wcrt/crt0.s.o $(WORSTC_BUILD_DIR)/wcrt/wcrbegin.o 
	install -t $(SYSTEM_ROOT)/System/Libraries $(WORSTC_BUILD_DIR)/wcrt/wcrbegin.o

wcrt: $(WCRT_BUILD)

$(WORSTC_BUILD_DIR)/wcrt/%.s.o: $(WORSTC_SOURCE_DIR)/wcrt/%.s
	mkdir -p $(WORSTC_BUILD_DIR)/wcrt
	$(AS) $< -g -o $@
