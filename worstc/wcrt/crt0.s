/* crt0.s
   Purpose: Executable start-up */

.section .text

.global _start
_start:
    push %rbp
    mov %rsp, %rbp

    cld
    mov $main, %rdi
    mov $_init, %rsi
    mov $_fini, %rdx
    
    /* Required for initfiniarray. */ 
    mov $__preinit_array_start, %rcx
    mov $__preinit_array_end, %r8
    mov $__init_array_start, %r9
    push $__fini_array_end
    push $__fini_array_start
    push $__init_array_end

    call __libc_call_main@plt

    hlt /* Exit somehow. */
